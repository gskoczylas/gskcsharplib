﻿/**********************************************
 *                                            *
 *                  GSkC#Lib                  *
 *     ——————————————————————————————————     *
 *  Copyright © 2009-2025 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************/


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace GSkTools.INI {

   /// <summary>
   /// Działania na pliku konfiguracyjnym.
   /// </summary>
   /// <remarks>
   /// W przypadku pojedynczych działań użycie klasy <see cref="IniUtils"/>
   /// może być wygodniejsze.
   /// </remarks>
   public class IniFile : IDisposable {

      #region Private definitions

      #region Private fields

      private readonly string Filename = "";

      private readonly List<string> Config = new List<string>();

      private bool Disposed = false;

      private int UpdateLevelValue = 0;

      #endregion Private fields

      /// <summary>
      /// Czy wskazany wiersz jest definicją sekcji.
      /// </summary>
      /// <param name="Line">Sprawdzany wiersz.</param>
      /// <remarks>Sekcja to tekst rozpoczynający się znakiem „[”
      /// i kończący się znakiem „]”. Między tymi znakami może, 
      /// ale nie musi, być jakiś tekst.</remarks>
      private static bool IsSection(string Line) {
         return Line.StartsWith("[")
                && Line.EndsWith("]");
      }

      /// <summary>
      /// Czy wskazany wiersz jest definicją zmiennej.
      /// </summary>
      /// <param name="Line">Sprawdzany wiersz.</param>
      /// <remarks>Zmienna to tekst rozpoczynający się literą lub cyfrą
      /// i zawierający znak „=”.</remarks>
      private static bool IsVariable(string Line) {
         bool Result;
         if (Result = Line.Length > 0) {
            char FirstChar = Line.ToUpper()[0];
            Result = (FirstChar >= 'A' && FirstChar <= 'Z'
                      || FirstChar >= '0' && FirstChar <= '9')
                     && Line.Contains("=");
         }
         return Result;
      }

      /// <summary>
      /// Szuka sekcję.
      /// </summary>
      /// <param name="SectionName">Nazwa szukanej sekcji.</param>
      /// <returns>Numer wiersza, w którym rozpoczyna się szukana sekcja.
      /// Jeżeli takiej sekcji nie ma, to wynikiem jest <c>-1</c>.</returns>
      /// <remarks>Pierwszy wiersz w pliku ma numer <c>0</c>.</remarks>
      private int Find(string SectionName) {
         string Line;
         int LineNo = 0;
         do {
            Line = Config[LineNo];
            if (Line.StartsWith("[")
                && Line.EndsWith("]")
                && Line.Substring(1, Line.Length - 2).Equals(SectionName, StringComparison.CurrentCultureIgnoreCase))
               return LineNo;
            LineNo++;
         } while (LineNo <= Config.Count);
         return -1;
      }

      /// <summary>
      /// Szuka zmienną
      /// </summary>
      /// <param name="SectionName">Sekcja zawierająca szukaną zmienną.</param>
      /// <param name="VariableName">Nazwa szukanej zmiennej.</param>
      /// <returns>Numer wiersza zawierającego definicję szukanej zmiennej.
      /// Jeżeli takiej sekcji lub zmiennej nie ma, to wynikiem jest <c>-1</c>.</returns>
      /// <remarks>Pierwszy wiersz w pliku ma numer <c>0</c>.</remarks>
      private int Find(string SectionName,
                       string VariableName) {
         string Line;
         int LineNo = Find(SectionName);
         if (LineNo >= 0) {
            do {
               Line = Config[LineNo];
               if (Line.Length > 0
                   && (Line[0] >= 'A' && Line[0] <= 'Z'
                       || Line[0] >= '0' && Line[0] <= '9')
                   && Line.Contains("=")) {
                  if (Line.Substring(0, Line.IndexOf('=')).TrimEnd().Equals(VariableName, StringComparison.CurrentCultureIgnoreCase))
                     return LineNo;
               }
               else if (Line.StartsWith("[") && Line.EndsWith("]")) {
                  break;
               }
               LineNo++;
            } while (LineNo <= Config.Count);
         }
         return -1;
      }

      /// <summary>
      /// Saves configuration to the file, if not saved yet.
      /// </summary>
      private void Dispose(bool ExpliciteCall) {
         if (Disposed) {
            if (ExpliciteCall) {
               if (Modified) {
                  Save();
               }
            }       
            Disposed = true;
         }
      }

      #endregion Private definitions

      #region Public properties

      /// <summary>
      /// Nazwa pliku konfiguracyjnego.
      /// </summary>
      /// <remarks>
      /// <para>Ścieżkę do pliku konfiguracyjnego można wskazać w konstruktorze 
      /// klasy.</para>
      /// 
      /// <para>Wskazany plik nie musi istnieć. Jeżeli nie istnieje, to wszystkie
      /// odczytywane zmienne będą mieć wartości domyślne.</para>
      /// 
      /// <para>Pliki konfiguracyjne zazwyczaj mają rozszerzenie „.ini”.
      /// Klasa <c>IniFile</c> nie sprawdza ani nie dodaje standardowego rozszerzenia 
      /// nazwy pliku konfiguracyjnego.</para>
      /// </remarks>
      public string FileName {
         get {
            return Filename;
         }
      }

      /// <summary>
      /// Czy konfiguracja jest zmodyfikowana.
      /// </summary>
      /// <remarks>
      /// Standardowo każda modyfikacja konfiguracji jest automatycznie zapisywana
      /// do pliku, chyba że została wywołana metoda <see cref="BeginUpdate"><c>BeginUpdate</c></see>.
      /// </remarks>
      public bool Modified {
         get;
         set;
      }

      /// <summary>
      /// Poziom wywołania metod <see cref="BeginUpdate"/>/<see cref="EndUpdate"/>.
      /// </summary>
      /// <remarks>
      /// Każde wywołanie metody <see cref="BeginUpdate"><c>BeginUpdate()</c></see> 
      /// zwiększa tę właściość, a każde wywołanie metody <see cref="EndUpdate"><c>EndUpdate()</c></see>
      /// zmniejsza tę własciwość.
      /// </remarks>
      public int UpdateLevel {
         get {
            return UpdateLevelValue;
         }
      }

      #endregion Public properties

      #region IDisposable implementation

      public void Dispose() {
         Dispose(true);
         GC.SuppressFinalize(this);
      }

      #endregion IDisposable implementation

      #region Konstruktory i destruktor

      /// <summary>
      /// Konstruktor klasy.
      /// </summary>
      /// <param name="Filename">Nazwa pliku konfiguracyjnego.</param>
      /// <remarks>
      /// <para>Wskazany plik nie musi istnieć. Jeżeli nie istnieje, to wszystkie
      /// odczytywane zmienne będą mieć wartości domyślne.</para>
      /// <para>Pliki konfiguracyjne zazwyczaj mają rozszerzenie „.ini”.
      /// Klasa <c>IniFile</c> nie sprawdza ani nie dodaje standardowego rozszerzenia 
      /// nazwy pliku konfiguracyjnego.</para>
      /// </remarks>
      public IniFile(string Filename) {
         Debug.Assert(Filename != "");
         this.Filename = Filename;
         Load();
         Modified = false;
      }

      ~IniFile() {
         Dispose(false);
      }

      #endregion Konstruktory i destruktor

      /// <summary>
      /// Odczytuje plik konfiguracyjny z dysku.
      /// </summary>
      /// <remarks>
      /// <para>Zawartość pliku konfiguracyjnego jest automatycznie odczytywana
      /// z dysku podczas tworzenia instancji klasy.</para>
      /// <para>Jeżeli są niezapisane modyfikacje konfiguracji, to zostaną one utracone.</para>
      /// </remarks>
      /// <seealso cref="Save"/>
      /// <seealso cref="BeginUpdate"/>
      /// <seealso cref="EndUpdate"/>
      public void Load() {
         using (StreamReader Input = new StreamReader(Filename)) {
            while (!Input.EndOfStream) {
               Config.Add(Input.ReadLine().Trim());
            }
         }
         Modified = false;
      }

      /// <summary>
      /// Zapisuje konfigurację do <see cref="Filename">pliku</see>.
      /// </summary>
      /// <remarks>Zawartość pliku konfiguracyjnego jest automatycznie odczytywana
      /// z dysku podczas tworzenia instancji klasy.</remarks>
      /// <seealso cref="Load"/>
      /// <seealso cref="BeginUpdate"/>
      /// <seealso cref="EndUpdate"/>
      public void Save() {
         using (StreamWriter Output = new StreamWriter(Filename, false)) {
            foreach (string Line in Config) {
               Output.WriteLine(Line);
            }
         }
         Modified = false;
      }

      /// <summary>
      /// Rozpoczyna modyfikacje konfiguracji.
      /// </summary>
      /// <remarks>
      /// <para>Standardowo każda zmiana konfiguracji jest natychmiast zapisywana do 
      /// <see cref="Filename">pliku</see>. Jeżeli aplikacja potrzebuje zmienić więcej
      /// więcej parametrów konfiguracyjnych, to warto zapisać je jednorazowo po zrobieniu
      /// wszystkich modyfikacji. W tym celu na początku należy wywołać metodę 
      /// <c>BeginUpdate()</c>. Po dokonaniu wszystkich modyfikacji należy wywołać metodę 
      /// <see cref="EndUpdate"><c>EndUpdate()</c></see>.</para>
      /// 
      /// <para>Każdemu wywołaniu <c>BeginUpdate()</c> musi odpowiadać wywołanie 
      /// <see cref="EndUpdate">EndUpdate()</see>.</para>
      /// 
      /// <para>Aktualny poziom wywołania <c>BeginUpdate</c>/<see cref="EndUpdate"/> można sprawdzić
      /// badając właściwość <see cref="UpdateLevel"/>.</para>
      /// </remarks>
      /// 
      /// <seealso cref="EndUpdate"/>
      /// <seealso cref="UpdateLevel"/>
      public void BeginUpdate() {
         Interlocked.Increment(ref UpdateLevelValue);
      }

      /// <summary>
      /// Kończy modyfikacje konfiguracji.
      /// </summary>
      /// <remarks>
      /// <para>Standardowo każda zmiana konfiguracji jest natychmiast zapisywana do 
      /// <see cref="Filename">pliku</see>. Jeżeli aplikacja potrzebuje zmienić więcej
      /// więcej parametrów konfiguracyjnych, to warto zapisać je jednorazowo po zrobieniu
      /// wszystkich modyfikacji. W tym celu na początku należy wywołać metodę 
      /// <see cref="BeginUpdate">BeginUpdate()</see>. Po dokonaniu wszystkich modyfikacji 
      /// należy wywołać metodę <c>EndUpdate()</c>.</para>
      /// 
      /// <para>Każdemu wywołaniu <c>BeginUpdate()</c> musi odpowiadać wywołanie 
      /// <see cref="EndUpdate">EndUpdate()</see>.</para>
      /// 
      /// <para>Aktualny poziom wywołania <c>BeginUpdate</c>/<see cref="EndUpdate"/> można sprawdzić
      /// badając właściwość <see cref="UpdateLevel"/>.</para>
      /// </remarks>
      /// 
      /// <seealso cref="EndUpdate"/>
      /// <seealso cref="UpdateLevel"/>
      public void EndUpdate() {
         Debug.Assert(UpdateLevelValue > 0);
         if (Interlocked.Decrement(ref UpdateLevelValue) == 0) {
            if (Modified) {
               Save();
            }
         }
      }

      public string ReadValue(string Section,
                              string Key,
                              string Default) {
         int LineNo = Find(Section, Key);
         if (LineNo >= 0) {
            string Line = Config[LineNo];
            return Line.Substring(Line.IndexOf('=') + 1).TrimStart();
         }
         else
            return Default;
      }

      public int ReadValue(string Section,
                           string Key,
                           int Default) {
         return int.Parse(ReadValue(Section, Key, Default.ToString()));
      }

      public bool ReadValue(string Section,
                            string Key,
                            bool Default) {
         return bool.Parse(ReadValue(Section, Key, Default.ToString()));
      }

      public void WriteValue(string Section,
                             string Key,
                             string Value) {
         BeginUpdate();
         try {
            int LineNo = Find(Section, Key);
            if (LineNo >= 0)
               Config[LineNo] = Key + '=' + Value;
            else {
               LineNo = Find(Section);
               if (LineNo >= 0)
                  Config.Insert(LineNo + 1, Key + '=' + Value);
               else {
                  Config.Add('[' + Section + ']');
                  Config.Add(Key + '=' + Value);
               }
            }
            Modified = true;
         }
         finally {
            EndUpdate();
         }
      }

      public void WriteValue(string Section,
                             string Key,
                             int Value) {
         WriteValue(Section, Key, Value.ToString());
      }

      public void WriteValue(string Section,
                             string Key,
                             bool Value) {
         WriteValue(Section, Key, Value.ToString());
      }

      /// <summary>
      /// Usuwa wskazaną zmienną.
      /// </summary>
      /// <param name="Section">Nazwa sekcji.</param>
      /// <param name="Key">Nazwa usuwanej zmiennej.</param>
      /// <remarks>
      /// Jeżeli po usunięciu zmiennej w sekcji nie ma więcej zmiennych,
      /// to usuwana jest cała sekcja.
      /// </remarks>
      public void DeleteKey(string Section,
                            string Key) {
         BeginUpdate();
         try {
            int LineNo = Find(Section, Key);
            if (LineNo >= 0) {
               Config.RemoveAt(LineNo);
               Modified = true;
               /* Usuwam sekcję, jeżeli pusta */
               LineNo = Find(Section);
               Debug.Assert(LineNo >= 0);
               while (++LineNo < Config.Count) {
                  if (IsVariable(Config[LineNo]))
                     return;
                  if (IsSection(Config[LineNo]))
                     break;
               }
               /* Jeżeli w sekcji jest jakakolwiek zmienna, to w pętli jest wyjście z tej procedury.
                * Skoro dotarł tutaj, to znaczy, że sekcja jest pusta. */
               DeleteSection(Section);
            }
         }
         finally {
            EndUpdate();
         }
      }

      public void DeleteSection(string Section) {
         BeginUpdate();
         try {
            int StartLineNo = Find(Section);
            if (StartLineNo >= 0) {
               int EndLineNo = StartLineNo;
               while (++EndLineNo < Config.Count) {
                  if (IsSection(Config[EndLineNo]))
                     break;
               }
               Config.RemoveRange(StartLineNo, EndLineNo - StartLineNo);
               Modified = true;
            }
         }
         finally {
            EndUpdate();
         }
      }

      public void ReadSections(List<string> Sections) {
         Sections.Clear();
         foreach (string Line in Config)
            if (IsSection(Line))
               Sections.Add(Line.Substring(1, Line.Length - 2));
      }

      public void ReadSectionKeys(string Section,
                                  List<string> Keys) {
         int LineNo;
         Keys.Clear();
         if ((LineNo = Find(Section)) >= 0) {
            while (++LineNo < Config.Count) {
               string Line = Config[LineNo];
               if (IsVariable(Line))
                  Keys.Add(Line.Substring(0, Line.IndexOf('=')).TrimEnd());
               else if (IsSection(Line))
                  break;
            }
         }
      }

      public void ReadSectionValues(string Section,
                                    Dictionary<string, string> Values) {
         int LineNo;
         Values.Clear();
         if ((LineNo = Find(Section)) >= 0) {
            while (++LineNo < Config.Count) {
               string Line = Config[LineNo];
               if (IsVariable(Line))
                  Values.Add(Line.Substring(1, Line.IndexOf('=')).TrimEnd(),
                             Line.Substring(Line.IndexOf('=') + 1).TrimStart());
               else if (IsSection(Line))
                  break;
            }
         }
      }

      public bool SectionExists(string Section) {
         return Find(Section) >= 0;
      }

      public bool ValueExists(string Section,                  
                              string Key) {
         return Find(Section, Key) >= 0;
      }

   }

   /// <summary>
   /// Klasa do pojedynczych działań na pliku konfiguracyjnym.
   /// </summary>
   /// <remarks>
   /// W przypadku bardziej złożonych działań lepiej jest użyć klasę <see cref="IniFile"/>.
   /// </remarks>
   static public class IniUtils {

      static public string ReadValue(string Filename,
                                     string Section,
                                     string Key,
                                     string Default) {
         using (IniFile Conf = new IniFile(Filename)) {
            return Conf.ReadValue(Section, Key, Default);
         }
      }

      static public int ReadValue(string Filename,
                                  string Section,
                                  string Key,
                                  int    Default) {
         using (IniFile Conf = new IniFile(Filename)) {
            return Conf.ReadValue(Section, Key, Default);
         }
      }

      static public bool ReadValue(string Filename,
                                   string Section,
                                   string Key,
                                   bool   Default) {
         using (IniFile Conf = new IniFile(Filename)) {
            return Conf.ReadValue(Section, Key, Default);
         }
      }

      static public void WriteValue(string Filename,
                                    string Section,
                                    string Key,
                                    string Value) {
         using (IniFile Conf = new IniFile(Filename)) {
            Conf.WriteValue(Section, Key, Value);
         }
      }

      static public void WriteValue(string Filename,
                                    string Section,
                                    string Key,
                                    int    Value) {
         using (IniFile Conf = new IniFile(Filename)) {
            Conf.WriteValue(Section, Key, Value);
         }
      }

      static public void WriteValue(string Filename,
                                    string Section,
                                    string Key,
                                    bool   Value) {
         using (IniFile Conf = new IniFile(Filename)) {
            Conf.WriteValue(Section, Key, Value);
         }
      }

   }
}
