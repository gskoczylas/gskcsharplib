﻿/**********************************************
 *                                            *
 *                  GSkC#Lib                  *
 *     ——————————————————————————————————     *
 *  Copyright © 2009-2025 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************/


using System;

namespace GSkTools.General
{
   /// <summary>
   ///   Definiuje możliwy wynik zwracany przez funkcje <c>CompareValues</c>.
   /// </summary>
   /// <remarks>
   ///   Wartości mają następujące znaczenie:
   ///   <list type="bullet">
   ///     <item>
   ///       <term>LessThanValue</term>
   ///       <description>
   ///         Pierwsza wartość jest mniejsza (wcześniejsza) od drugiej wartości.
   ///       </description>
   ///       <term>EqualToValue</term>
   ///       <description>
   ///         Pierwsza wartość jest równa (taka sama jak) druga wartość.
   ///       </description>
   ///       <term>GreaterThanValue</term>
   ///       <description>
   ///         Pierwsza wartość jest większa (późniejsza) od drugiej wartości.
   ///       </description>
   ///     </item>
   ///   </list>
   ///   <seealso cref="GSkTools.Math.MathUtils.CompareValues"/>
   /// </remarks>
   public enum Relationship
   {
      LessThanValue, EqualToValue, GreaterThanValue
   }

   /// <summary>
   /// Funkcje ogólnego przeznaczenia
   /// </summary>
   static public class GeneralUtils
   {
      #region Czy pole w bazie jest puste

      /// <summary>
      /// Sprawdza, czy pole jest puste.
      /// </summary>
      /// <param name="Field">Sprawdzane pole.</param>
      /// <returns>Wartość typu <c>bool</c>.</returns>
      /// <remarks>Pole uważane jest za puste jeżeli jest równe <c>null</c>
      /// lub jest równe wartości <c>System.DBNull</c>.</remarks>
      static public bool FieldIsEmpty(object Field)
      {
         return Field == null
                || Field == System.DBNull.Value;
      }


      /// <summary>
      /// Zwraca wartość pola bazy danych wskazanego w parametrze.
      /// Jeżeli wartość jest równa <c>DBNull</c> to wynikiem jest <c>null</c>.
      /// </summary>
      /// <param name="Field">Analizowane pole.</param>
      /// <returns></returns>
      static public object DbField(object Field)
      {
         return Field == System.DBNull.Value ? null : Field;
      }

      #endregion

      #region Odmiana

      /// <summary>
      /// Funkcja zwraca odpowiedni tekst, w zależności od przekazanej wartości.
      /// </summary>
      /// <param name="nIle">Wartość, według której wybierany jest napis, który
      /// będzie wynikiem funkcji.</param>
      /// <param name="nWersja1">Wersja napisu dla ilości '1'.</param>
      /// <param name="nWersja2">Wersja napisu dla ilości '2'.</param>
      /// <param name="nWersja5">Wersja napisu dla iliści '3'.</param>
      /// <returns>Jeden z napisów <paramref name="nWersja1"/>, 
      ///   <paramref name="nWersja2"/> lub <paramref name="nWersja3"/>,
      ///   w zależności od wartości parametru <paramref name="nIle"/>.
      /// </returns>
      /// <remarks>
      ///   <para>
      ///     Przykład użycia:
      ///     <example>
      ///       for (int i = 0; i <= 7; i++)
      ///         Console.WriteLine(i.ToString() 
      ///                           + ' ' 
      ///                           + Odmiana(i, 'złoty', 'złote', 'złotych'));
      ///     </example>
      ///     Powyższy przykład wyświetli następujące teksty:
      ///     <example>
      ///       0 złotych
      ///       1 złoty
      ///       2 złote
      ///       3 złote
      ///       4 złote
      ///       5 złotych
      ///       6 złotych
      ///       7 złotych
      ///     </example>
      ///   </para>
      ///   <para>
      ///     Jeżeli teksty <paramref name="Wersja2"/> i <paramref name="Wersja5"/>
      ///     są identyczne, to ostatni parametr można pominąć.
      ///   </para>
      /// </remarks>
      static public string Odmiana(int nIle,
                                   string nWersja1,
                                   string nWersja2,
                                   string nWersja5)
      {
         if (nIle == 1)
            return nWersja1;
         else {
            int nJdn = nIle % 10;
            int nDzs = nIle % 100;
            if (nJdn >= 2 && nJdn <= 4
                && (nDzs < 12 || nDzs > 14))
               return nWersja2;
            else
               return nWersja5;
         }
      }

      /// <summary>
      /// Funkcja zwraca odpowiedni tekst, w zależności od przekazanej wartości.
      /// </summary>
      /// <param name="nIle">Wartość, według której wybierany jest napis, który
      /// będzie wynikiem funkcji.</param>
      /// <param name="nWersja1">Wersja napisu dla ilości '1'.</param>
      /// <param name="nWersja2">Wersja napisu dla ilości '2'.</param>
      /// <returns>Jeden z napisów <paramref name="nWersja1"/>, 
      ///   <paramref name="nWersja2"/>, w zależności od wartości parametru 
      ///   <paramref name="nIle"/>.
      /// </returns>
      /// <remarks>
      ///   <para>
      ///     Przykład użycia:
      ///     <example>
      ///       for (int i = 0; i <= 5; i++)
      ///         Console.WriteLine(i.ToString() + ' ' + Odmiana(i, 'dzień', 'dni'));
      ///     </example>
      ///     Powyższy przykład wyświetli następujące teksty:
      ///     <example>
      ///       0 dni
      ///       1 dzień
      ///       2 dni
      ///       3 dni
      ///       4 dni
      ///       5 dni
      ///     </example>
      ///   </para>
      ///   <para>
      ///     Jeżeli tekst dla ilości '5' jest inny niż dla ilości '2', to
      ///     należy użyć wersję funkcji z trzema napisami.
      ///   </para>
      /// </remarks>
      static public string Odmiana(int nIle,
                                   string nWersja1,
                                   string nWersja2)
      {
         return Odmiana(nIle, nWersja1, nWersja2, nWersja2);
      }

      #endregion

   }
}
