﻿/**********************************************
 *                                            *
 *                  GSkC#Lib                  *
 *     ——————————————————————————————————     *
 *  Copyright © 2009-2025 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using GSkTools.StringUtils;

/// <summary>
/// Analizator opcji z linii poleceń aplikacji.
/// </summary>
namespace GSkTools.CommandLineOptions
{

   /// <summary>
   /// Abstrakcyjna kalasa bazowa dla opcji.
   /// </summary>
   /// <remarks>
   /// Ta klasa implementuje podstawowy zestaw metod i właściwości wykorzystywanych
   /// przez klasę <see cref="CmdLineOptionParser"/>. Definiuje również właściwość
   /// pozwlajającą definiować opis opcji.
   /// </remarks>
   public abstract class CustomOption
   {
      /// <summary>
      /// Standardowy przedrostek opcji w wersji jednoznakowej.
      /// </summary>
      public const char DefShortOptionPrefix = '-';

      /// <summary>
      /// Standardowy przedrostek opcji w wersji wieloznakowej.
      /// </summary>
      public const string DefLongOptionPrefix = "--";

      #region Private Fields

      internal CmdLineOptionParser Parser;

      #endregion
      
      #region Properites

      /// <summary>
      /// Informuje o długości opcji.
      /// </summary>
      /// <remarks>
      /// Ta właściwość informuje o liczbie znaków w napisie "-s --long", gdzie
      /// <c>s</c> oznacza jednoznakową nazwę opcji, a <c>long</c> oznacza
      /// wieloznakową nazwę opcji.
      /// </remarks>
      public int OptionWidth {
         get {
            int nResult = 0;
            if (ShortName != '\0')
               nResult += 4;   // "-x, "
            if (LongName != null
                && LongName.Length > 0)
               nResult += LongName.Length + Parser.LongOptionPrefix.Length;
            else
               nResult -= 2;
            return nResult;
         }
      }

      /// <summary>
      /// Jednoznakowa nazwa opcji.
      /// </summary>
      /// <remarks>
      /// Jeżeli dana opcja nie ma nazwy jednoznakowej, to ta właściwość ma
      /// wartość '\x00' (znak NUL).
      /// </remarks>
      public char ShortName {
         get; set;
      }

      /// <summary>
      /// Wieloznakowa nazwa opcji.
      /// </summary>
      /// <remarks>
      /// Jeżeli dana opcja nie ma wieloznakowej nazwy, to ta właściwość albo
      /// ma wartość <c>null</c>, albo jest pustym napisem.
      /// </remarks>
      public string LongName {
         get; set;
      }

      /// <summary>
      /// Czy wielkość liter w jednoznakowej nazwie opcji ma znaczenie.
      /// </summary>
      /// <remarks>Domyślnie wielkość liter nie ma znaczenia.</remarks>
      public bool ShortCaseSensitive {
         get; set;
      }

      /// <summary>
      /// Czy wielkość liter w wieloznakowej nazwie opcji ma znaczenie.
      /// </summary>
      /// <remarks>Domyślnie wielkość liter nie ma znaczenia.</remarks>
      public bool LongCaseSensitive {
         get; set;
      }

      /// <summary>
      /// Informuje, czy opcja została użyta w wierszu poleceń aplikacji.
      /// </summary>
      public bool WasSpecified {
         get;
         internal set;
      }

      /// <summary>
      /// Opis opcji.
      /// </summary>
      /// <remarks>
      /// Ten opis można na przykład wykorzystać do wyświetlenia listy dostępnych
      /// opcji wraz z ich krótkimi opisami.
      /// </remarks>
      public string Explanation {
         get; set;
      }

      #endregion

      /// <summary>
      /// Metoda wykorzystywana wewnętrznie.
      /// </summary>
      internal abstract bool ParseOption(StringCollection Words);
      
      protected void AddVal(ref string sText,
                           string sName, 
                           string sValue,
                           ref string sSep)
      {
         sText = sText + sSep + sName + (" " + sValue).TrimEnd();
         sSep = ", ";
      }
      
      public override string ToString()
      {
         string sResult = this.GetType().FullName;
         string sSep = "[";
         if (LongName != null
             && LongName != "")
            AddVal(ref sResult, 
                   "LongName", LongName, 
                   ref sSep);
         if (ShortName != '\0')
            AddVal(ref sResult, 
                   "ShortName", ShortName.ToString(),
                   ref sSep);
         if (LongCaseSensitive)
            AddVal(ref sResult, 
                   "LongCaseSensitive", LongCaseSensitive.ToString(), 
                   ref sSep);
         if (ShortCaseSensitive)
            AddVal(ref sResult,
                   "ShortCaseSensitive", ShortCaseSensitive.ToString(),
                   ref sSep);
         AddVal(ref sResult,
                WasSpecified ? "was" : "wasn't", "specified",
                ref sSep);
         return sResult + "]";
      }

      /// <summary>
      /// Metoda wykorzystywana wewnętrznie.
      /// </summary>
      protected string FormatExplanation(int nOptWidth,
                                         bool bWrapText)
      {
         StringBuilder sResult = new StringBuilder("  ");
         int nWritten = 2;

         if (ShortName != '\0') {
            sResult.Append(Parser.ShortOptionPrefix.ToString() + ShortName.ToString());
            nWritten += 2;
            if (LongName != null
                && LongName.Length != 0) {
               sResult.Append(", ");
               nWritten += 2;
            }
         }
         if (LongName != null
             && LongName.Length != 0) {
            sResult.Append(Parser.LongOptionPrefix + LongName);
            nWritten += Parser.LongOptionPrefix.Length + LongName.Length;
         }
         sResult.Append(" --> ");
         nWritten += 5;
         if (bWrapText) {
            string[] asLines = Explanation.Wrap(77 - nOptWidth);
            for (int nInd = 0; nInd < asLines.Length; nInd++) {
               if (asLines[nInd].Length > 0) {
                  if (nInd == 0)
                     sResult.Append(StrUtils.Spaces(nOptWidth + 4 - nWritten));
                  else
                     sResult.Append(StrUtils.Spaces(nOptWidth + 4));
               }
               sResult.AppendLine(asLines[nInd]);
            }
         } else {
            sResult.AppendLine(Explanation);
         }
         return sResult.ToString();
      }

      /// <summary>
      /// Zwraca opis opcji.
      /// </summary>
      /// <param name="nOptWidth">Szerokość kolumny dla nazw opcji.</param>
      /// <returns>Wynikiem jest odpowiednio sformatowany opis.</returns>
      /// <remarks>
      /// Ta metoda jest wykorzystywana do sformatowania opisu wszystkich opcji
      /// aplikacji. Taki opis składa się z dwóch kolumn. W pierwszej są nazwy
      /// opcji, a w drugiej — ich opisy. Parametr <paramref name="nOptWidth"/>
      /// informuje o szerokości pierwszej kolumny (dla nazw opcji).
      /// </remarks>
      public string GetExplanation(int nOptWidth)
      {
         return FormatExplanation(nOptWidth, false);
      }

      /// <summary>
      /// Wyświetla na konsoli opis opcji.
      /// </summary>
      /// <param name="nOptWidth">Szerokość kolumny dla nazw opcji.</param>
      /// <remarks>
      /// Ta metoda jest wykorzystywana do wyświetlenia na konsoli znakowej
      /// sformatowanego opisu wszystkich opcji aplikacji. Taki opis składa się
      /// z dwóch kolumn. W pierwszej są nazwy opcji, a w drugiej — ich opisy.
      /// Parametr <paramref name="nOptWidth"/> informuje o szerokości pierwszej
      /// kolumny (dla nazw opcji).
      /// </remarks>
      public void WriteExplanation(int nOptWidth)
      {
         System.Console.Write(FormatExplanation(nOptWidth, true));
      }
   }

   /// <summary>
   /// Prosta opcja logiczna.
   /// </summary>
   /// <remarks>
   /// Tego typu opcja nie przyjmuje wartości. Jej właściwość <c>TrunedOn</c>
   /// informuje, czy opcja wystąpiła w wierszu poleceń aplikacji.
   /// </remarks>
   public class BoolOption : CustomOption
   {
      /// <summary>
      /// Informuje, czy opcja została użyta w wierszu poleceń aplikacji.
      /// </summary>
      /// <remarks>Taką samą wartość zwraca właściwość <c>WasSpecified</c>.</remarks>
      public bool TurnedOn {
         get {
            return WasSpecified;
         }
      }

      /// <summary>
      /// Metoda wykorzystywana wewnętrznie.
      /// </summary>
      internal override bool ParseOption(StringCollection Words)
      {
         bool bResult = false;
         string sWord = Words[0];
         if (ShortName != '\0') {
            if (sWord.Length == 2
                && sWord[0] == Parser.ShortOptionPrefix
                && (sWord[1] == ShortName
                    || !ShortCaseSensitive
                    && Char.ToLower(sWord[1]) == Char.ToLower(ShortName))) {
               bResult = true;
               Words.RemoveAt(0);
               WasSpecified = true;
            }
         }
         if (!bResult
             && LongName != null
             && LongName.Length > 0) {
            if (sWord == Parser.LongOptionPrefix + LongName
                || (!LongCaseSensitive
                    && sWord.ToLower() == Parser.LongOptionPrefix + LongName.ToLower())) {
               bResult = true;
               Words.RemoveAt(0);
               WasSpecified = true;
            }
         }
         return bResult;
      }
   }

   /// <summary>
   /// Bazowa klasa dla opcji z wartością.
   /// </summary>
   /// <remarks>
   /// Jest to klasa bazowa dla wszystkich opcji mogących mieę jedną lub więcej
   /// wartości. Opcje od ich wartości oddziela albo odstęp, albo znak <c>=</c>.
   /// Wartość może również występować bezpośrednio doklejona do nazwy opcji.
   /// </remarks>
   /// <example>
   ///   --option value
   ///   --option=value
   /// </example>
   public abstract class CustomValueOption : CustomOption
   {
      /// <summary>
      /// Wartość opcji.
      /// </summary>
      /// <remarks>
      /// <para>Ta właściwość jest przedefiniowywana przez klasy definiujące
      /// poszczególne typy opcji.</para>
      /// <para>Wartość domyślną opcji należy wstawić do tej właściwości przed
      /// parsowaniem parametrów wiersza poleceń.</para>
      /// </remarks>
      public object Value {
         get; set;
      }

      /// <summary>
      /// Metoda wykorzystywana wewnętrznie.
      /// </summary>
      protected abstract bool CheckValue(string sText);

      /// <summary>
      /// Metoda wykorzystywana wewnętrznie.
      /// </summary>
      internal override bool ParseOption(StringCollection Words)
      {
         string sValue;
         bool bResult = false;
         string sWord = Words[0];
         if (ShortName != '\0') {
            if (sWord.Length == 2
                && sWord[0] == Parser.ShortOptionPrefix
                && (sWord[1] == ShortName
                    || !ShortCaseSensitive
                    && Char.ToLower(sWord[1]) == Char.ToLower(ShortName))) {
               sValue = sWord.Substring(2);
               if (sValue == "") {
                  if (Words.Count > 1) {
                     sValue = Words[1];
                     if (CheckValue(sValue)) {
                        bResult = true;
                        Words.RemoveAt(0);
                        Words.RemoveAt(0);
                     } else {
                        bResult = CheckValue("");
                        if (bResult)
                           Words.RemoveAt(0);
                     }
                  }
               } else {
                  bResult = CheckValue(sValue);
                  if (bResult)
                     Words.RemoveAt(0);
               }
            }
         }
         if (!bResult
             && LongName != null
             && LongName.Length != 0) {
            string sOption = sWord.Substring(0,
                                             Math.Min(Parser.LongOptionPrefix.Length + LongName.Length,
                                                      sWord.Length));
            if (sOption == Parser.LongOptionPrefix + LongName
                || !LongCaseSensitive
                && sOption.ToLower() == Parser.LongOptionPrefix + LongName.ToLower()) {
               if (sWord.Length == Parser.LongOptionPrefix.Length + LongName.Length) {
                  if (Words.Count > 1)
                     sValue = Words[1];
                  else
                     sValue = "";
                  bResult = CheckValue(sValue);
                  if (bResult) {
                     Words.RemoveAt(0);
                     if (Words.Count > 0)
                        Words.RemoveAt(0);
                  }
               } else {
                  if (sWord.Substring(Parser.LongOptionPrefix.Length + LongName.Length - 1, 1)== "=") {
                     sValue = sWord.Substring(Parser.LongOptionPrefix.Length + LongName.Length + 1);
                     bResult = CheckValue(sValue);
                     if (bResult)
                        Words.RemoveAt(0);
                  }
               }
            }
         }
         if (bResult)
            WasSpecified = true;
         return bResult;
      }
      
      protected abstract string FormatValue();
      
      public override string ToString()
      {
         string sResult = base.ToString();
         if (!sResult.EndsWith("[]"))
            sResult = sResult.Insert(sResult.Length - 2, ", ");
         return sResult.Insert(sResult.Length - 2, FormatValue());
      }
   }

   /// <summary>
   /// Wartościami opcji tego typu są liczby całkowite.
   /// </summary>
   public class IntegerOption : CustomValueOption
   {
      /// <summary>
      /// Wartość opcji.
      /// </summary>
      new public long Value {
         get {
            return (long)base.Value;
         }
         set {
            base.Value = value;
         }
      }

      /// <summary>
      /// Metoda wykorzystywana wewnętrznie.
      /// </summary>
      protected override bool CheckValue(string sText)
      {
         long nValue;
         bool bResult;
         if (bResult = long.TryParse(sText, out nValue))
            Value = nValue;
         return bResult;
      }
      
      protected override string FormatValue()
      {
         return Value.ToString();
      }
   }

   /// <summary>
   /// Wartościami opcji tego typu są napisy.
   /// </summary>
   /// <seealso cref="StringListOption"/>
   /// <seealso cref="SetOption"/>
   public class StringOption : CustomValueOption
   {
      /// <summary>
      /// Wartość opcji.
      /// </summary>
      new public string Value {
         get {
            return (string)base.Value;
         }
         set {
            base.Value = value;
         }
      }

      /// <summary>
      /// Metoda wykorzystywana wewnętrznie.
      /// </summary>
      protected override bool CheckValue(string sText)
      {
         Value = sText;
         return true;
      }
      
      protected override string FormatValue()
      {
         return "\"" + Value + "\"";
      }
   }

   /// <summary>
   /// Wartościami opcji tego typu są napisy. Może występować wielokrotnie.
   /// </summary>
   /// <remarks>
   /// W przypadku opcji typu <see cref="StringOption"/>, jeżeli dana opcja
   /// zostanie użyta więcej niż jeden raz, to wartością opcji jest wartość
   /// występująca ostatni raz. Natomiast jeżeli opcja <c>StringListOption</c>
   /// wystąpi wielokrotnie, to wszystkie wartości są dostępne poprzez właściwość
   /// <c>Variable</c> tej opcji.
   /// </remarks>
   /// <seealso cref="StringOption"/>
   /// <seealso cref="SetOption"/>
   public class StringListOption : CustomValueOption
   {
      /// <summary>
      /// Wartość opcji.
      /// </summary>
      new public StringCollection Value {
         get {
            return (StringCollection)base.Value;
         }
         set {
            base.Value = value;
         }
      }

      /// <summary>
      /// Metoda wykorzystywana wewnętrznie.
      /// </summary>
      protected override bool CheckValue(string sText)
      {
         Value.Add(sText);
         return true;
      }
      
      protected override string FormatValue()
      {
         return Value.ToString();
      }
   }

   /// <summary>
   /// Opcja reprezentuje zbiór wartości.
   /// </summary>
   /// <remarks>
   /// Opcja typu <c>SetOption</c> pozwala zdefiniować zbiór wartości. W wierszu
   /// poleceń definiuje się ją podobnie jak opcję typu <see cref="StringOption"/>
   /// lub <see cref="StringListOption"/>. Różni się od nich następującymi cechami:
   /// <list type="bullet">
   ///   <item>
   ///     <description>
   ///       Każda wartość może być poprzedzona znakiem <c>+</c> lub znakiem
   ///       <c>-</c>. Pierwszy znak oznacza dodanie wartości do zbioru wynikowego,
   ///       a drugi -- usunięcie wartości z wynikowego zbioru wartości.
   ///     </description>
   ///     <description>
   ///       Jeżeli wartość nie jest poprzedzona ani znakiem <c>+</c>, ani <c>-</c>,
   ///       to zbiór wartości jest resetowany (czyszczony), a wartość staje się
   ///       jedyną (w tym momencie) wartością zbioru wynikowego. Jest to przydatne
   ///       gdy aplikacja definiuje domyślny zestaw wartości, a w wierszu poleceń
   ///       chcemy zdefiniować zupełnie inny zestaw wartości.
   ///     </description>
   ///     <description>
   ///       W opcjach typu <see cref="StringListOption"</see> każde wystąpienie 
   ///       definiuje jedną wartość. Natomiast w opcjach typu <c>SetOption</c>
   ///       w jednym wystąpieniu opcji można zdefiniować dowolnie wiele wartości.
   ///       Wystarczy je rozdzielić przecinkami lub średnikami.
   ///     </description>
   ///   </item>
   /// </list>
   /// 
   /// W przypadku opcji typu <see cref="StringOption"/>, jeżeli dana opcja
   /// zostanie użyta więcej niż jeden raz, to wartością opcji jest wartość
   /// występująca ostatni raz. Natomiast jeżeli opcja <c>StringListOption</c>
   /// wystąpi wielokrotnie, to wszystkie wartości są dostępne poprzez właściwość
   /// <c>Variable</c> tej opcji.
   /// 
   /// Opcja typu <see cref="StringListOption"/> działa tak samo jak opcja
   /// <c>SetOption</c>, w której wszystkie wartości są poprzedzone znakiem 
   /// <c>+</c>.
   /// </remarks>
   /// <example>
   ///   --option+value1,+value2,-value3
   ///   --opt value3,+value4
   /// </example>
   /// <seealso cref="StringOption"/>
   /// <seealso cref="StringListOption"/>
   public class SetOption : CustomValueOption
   {
      new public StringCollection Value {
         get {
            return (StringCollection)base.Value;
         }
         set {
            base.Value = value;
         }
      }

      /// <summary>
      /// Wskazuje czy wielkość liter w zbiorze wartości ma znaczenie.
      /// </summary>
      /// <remarks>
      /// Jeżeli w parametrach tej opcji wystąpią dwie różne wartości różniące
      /// się wyłącznie wielkością liter, to w zależności od wartości tej
      /// właściwości będą rozróżniane, lub nie.
      /// </remarks>
      bool CaseSensitive {
         get; set;
      }

      /// <summary>
      /// Informuje, czy wskazana wartość jest jednym z elementów zbioru wartości.
      /// </summary>
      /// <param name="sValue">Badana wartość.</param>
      /// <param name="bCaseCensitive">Czy podczas szukania wartości wielkość
      /// liter powinna być brana pod uwagę.</param>
      /// <returns><c>true</c> jeżeli wskazana wartość jest jednym z elementów
      /// zbioru wartości.</returns>
      public bool HasValue(string sValue,
                           bool bCaseCensitive)
      {
         if (bCaseCensitive) {
            return Value.Contains(sValue);
         } else {
            foreach (string sVal in Value) {
               if (sVal.Equals(sValue, StringComparison.CurrentCultureIgnoreCase)) {
                  return true;
               }
            }
            return false;
         }
      }

      /// <summary>
      /// Informuje, czy wskazana wartość jest jednym z elementów zbioru wartości.
      /// </summary>
      /// <param name="sValue">Badana wartość.</param>
      /// <returns><c>true</c> jeżeli wskazana wartość jest jednym z elementów
      /// zbioru wartości.</returns>
      /// <remarks>
      /// Podczas szukania wartości brana jest pod uwagę wartość właściwości
      /// <see cref="CaseSensitive" />.
      /// </remarks>
      public bool HasValue(string sValue)
      {
         return HasValue(sValue, CaseSensitive);
      }

      /// <summary>
      /// Zbiór dopuszczalnych wartości.
      /// </summary>
      /// <remarks>
      /// Ta właściwość definiuje zbiór wartości możliwych do zaakceptowania
      /// przez daną opcję
      /// </remarks>
      public StringCollection PossibleValues {
         get; set;
      }
      
      public SetOption()
         : base()
      {
         PossibleValues = new StringCollection();
         Value = new StringCollection();
      }

      private class StringCollectionDupIgnore : StringCollection
      {
         public bool CaseSensitive
         {
            get; set;
         }

         public StringCollectionDupIgnore(bool bCaseSensitive)
            : base()
         {
            this.CaseSensitive = bCaseSensitive;
         }

         new private int IndexOf(string sValue)
         {
            if (CaseSensitive) {
               return base.IndexOf(sValue);
            } else {
               for (int nInd = 0; nInd < Count; nInd++) {
                  if (this[nInd].Equals(sValue, 
                                        StringComparison.CurrentCultureIgnoreCase)) {
                     return nInd;
                  }
               }
               return -1;
            }
         }

         new public int Add (string sValue)
         {
            int Result = this.IndexOf(sValue);
            if (Result < 0)
               Result = base.Add(sValue);
            return Result;
         }
      }

      /// <summary>
      /// Metoda wykorzystywana wewnętrznie.
      /// </summary>
      protected override bool CheckValue(string sText)
      {
         bool Result = true;
         bool bCleared = false;
         int nInd;
         StringCollectionDupIgnore lList = new StringCollectionDupIgnore(CaseSensitive);
         StringCollectionDupIgnore lResult = new StringCollectionDupIgnore(CaseSensitive);
         foreach (string sVal in sText.Split(',', ';'))
            lList.Add(sVal.Trim());
         foreach (string sVal in Value)
            lResult.Add(sVal);
         for (nInd = 0; nInd < lList.Count; nInd++) {
            string sBuf = lList[nInd];
            if (sBuf == "")
               continue;
            switch (sBuf[0]) {
               case '-':
                  sBuf = sBuf.Remove(0, 1);
                  if (PossibleValues.IndexOf(sBuf) >= 0) {
                     lResult.Remove(sBuf);
                  } else {
                     Result = false;
                     break;
                  }
                  break;
               case '+':
                  sBuf = sBuf.Remove(0, 1);
                  if (PossibleValues.IndexOf(sBuf) >= 0) {
                     lResult.Add(sBuf);
                  } else {
                     Result = false;
                     break;
                  }
                  break;
               default:
                  if (sBuf[0] == '=') {
                     sBuf.Remove(0, 1);
                     if (sBuf == "")
                        continue;
                  }
                  if (PossibleValues.IndexOf(sBuf) >= 0) {
                     lResult.Add(sBuf);
                  } else {
                     Result = false;
                     break;
                  }
                  if (!bCleared) {
                     bCleared = true;
                     lResult.Clear();
                     nInd = -1;   // restart from beginning
                  }
                  break;
            }
         }
         Value = lResult;
         return Result;
      }
         
      protected override string FormatValue()
      {
         return Value.ToString();
      }
   }

   public class CmdLineOptionParser
   {
      #region Private Fields

      StringCollection CmdLineArgs = new StringCollection();
      StringCollection CmdLineLeftList = new StringCollection();
      List<CustomOption> OptionList = new List<CustomOption>();

      #endregion

      private void Initialize()
      {
         LongOptionPrefix = CustomOption.DefLongOptionPrefix;
         ShortOptionPrefix = CustomOption.DefShortOptionPrefix;
      }

      #region Constructors

      /// <summary>
      /// Konstruktor standardowy.
      /// </summary>
      /// <remarks>
      /// Użycie konstruktora standardowego powoduje, że analizowane są opcje
      /// wiersza poleceń aplikacji, pobrane poprzez <c>Environment.GetCommandLineArgs()</c>.
      /// </remarks>
      public CmdLineOptionParser()
      {
         Initialize();
         // Pobieram wszystkie parametry do lokalnej strunktury.
         // Jeżeli program jest wywołany spoza SharpDevelop, to pierwszym parametrem
         // jest ścieżka do programu.
         // Jeżeli program jest wywołany z SharpDevelop, to pierwszym parametrem
         // jest pusty napis.
         foreach(string sArg in Environment.GetCommandLineArgs())
            if (sArg != ""
                && !sArg.Equals(System.Windows.Forms.Application.ExecutablePath,
                                StringComparison.InvariantCultureIgnoreCase))
            CmdLineArgs.Add(sArg);
         // HACK Sprawdzić czy to działa również w aplikacjach innych niż WinForms
         // Zdaje mi się, że badanie System.Windows.Forms.Application.ExecutablePath
         // ma sens tylko w przypadku aplikacji konsolowych.
      }

      /// <summary>
      /// Konstruktor niestandardowy.
      /// </summary>
      /// <param name="Args">Parametry wiersza poleceń aplikacji.</param>
      /// <example>
      /// public MainForm(string[] args)
      /// {
      ///    CmdLineOptionParser = new CmdLineOptionParser(args);
      ///    ...
      /// }
      /// </example>
      public CmdLineOptionParser(string[] Args)
      {
         Initialize();
         foreach (string sArg in Args)
            if (sArg != "")
            CmdLineArgs.Add(sArg);
      }

      #endregion

      #region Properties

      /// <summary>
      /// Ciąg znaków będący prefiksem wieloznakowych nazw opcji.
      /// </summary>
      /// <remarks>
      /// Standardowo wieloznakowe nazwy opcji poprzedzane są znakami <c>--</c>.
      /// </remarks>
      public string LongOptionPrefix {
         get; set;
      }

      /// <summary>
      /// Znak będący prefiksem jednoznakowych nazw opcji.
      /// </summary>
      /// <remarks>
      /// Standardowo jednoznakowe nazwy opcji poprzedzone są znakiem <c>-</c>.
      /// </remarks>
      public char ShortOptionPrefix {
         get; set;
      }

      /// <summary>
      /// Liczba opcji aplikacji.
      /// </summary>
      /// <remarks>
      /// To jest liczba opcji rozpoznawanych przez aplikację. Natomiast informację
      /// czy dana opcja wystąpiła w parametrach wiersza poleceń aplikacji można
      /// sprawdzić badając właściwość <c>WasSpecified</c> poszczególnych opcji.
      /// </remarks>
      public int OptionCount {
         get {
            return OptionList.Count;
         }
      }

      /// <summary>
      /// Nierozpoznane fragmenty parametrów wiersza poleceń
      /// </summary>
      /// <remarks>
      /// Po zakończeniu analizy parametrów wiersza poleceń może się zdarzyć, że
      /// niektóre napisy nie zostaną rozpoznane jako opcje. Są one dostępne
      /// poprzez tę opcję.
      /// </remarks>
      public StringCollection CmdLineLeft {
         get {
            return CmdLineLeftList;
         }
      }

      #endregion

      #region Indexers

      /// <summary>
      /// Umożliwia dostęp do poszczególnych opcji poprzez indeks opcji.
      /// </summary>
      public CustomOption this[int nIndex] {
         get {
            return OptionList[nIndex];
         }
      }

      /// <summary>
      /// Umożliwia dostęp do poszczególnych opcji poprzez ich jednoznakową nazwę
      /// </summary>
      public CustomOption this[char cShortName] {
         get {
            char cAltShortName = Char.ToLower(cShortName);
            foreach (CustomOption Option in OptionList)
               if (Option.ShortName == cShortName
                   || (!Option.ShortCaseSensitive
                       && Char.ToLower(Option.ShortName) == cAltShortName))
               return Option;
            return null;
         }
      }

      /// <summary>
      /// Umożliwia dostęp do poszczególnych opcji poprzez ich wieloznakową nazwę
      /// </summary>
      public CustomOption this[string sLongName] {
         get {
            string sAltLongName = sLongName.ToLower();
            foreach (CustomOption Option in OptionList)
               if (Option.LongName == sLongName
                   || (!Option.LongCaseSensitive
                       && Option.LongName.ToLower() == sAltLongName))
               return Option;
            return null;
         }
      }

      #endregion

      /// <summary>
      /// Dodaje opcję do listy opcji aplikacji.
      /// </summary>
      /// <param name="Option">Opcja do dodania do listy.</param>
      /// <returns>Wynikiem metody jest dodawana opcja.</returns>
      public CustomOption AddOption(CustomOption Option)
      {
         OptionList.Add(Option);
         Option.Parser = this;
         return Option;
      }

      /// <summary>
      /// Analizuje parametry wiersza poleceń
      /// </summary>
      /// <remarks>
      /// <para>Metoda analizuje parametry wiersza poleceń. Jeżeli dana opcja
      /// zostanie rozpoznana, to jej właściwość <c>WasSpecified</c> ma wartość
      /// <c>true</c>. Jeżeli jest to opcja z wartością, to wartość opcji można
      /// pobrać poprzez właściwość <c>Variable</c>.</para>
      /// <para>Parametry wiersza poleceń, które nie zostały rozpoznane jako
      /// opcje lub ich wartości, dostępne są poprzez właściwość
      /// <c>CmdLineLeft</c>.</para>
      /// </remarks>
      public void ParseOptions()
      {
         if (LongOptionPrefix != null) {
            if (LongOptionPrefix.Length == 1)
               if (LongOptionPrefix[0] == ShortOptionPrefix)
               throw new ApplicationException("ShortOptionPrefix and LongOptionPrefix can not be equal");
         }
         bool bFoundSomething;
         StringCollection Params = new StringCollection();
         for (int nInd = 0; nInd < CmdLineArgs.Count; nInd++)
            Params.Add(CmdLineArgs[nInd]);
         CmdLineLeftList.Clear();
         while (Params.Count > 0) {
            bFoundSomething = false;
            foreach (CustomOption Option in OptionList) {
               bFoundSomething = Option.ParseOption(Params);
               if (bFoundSomething)
                  break;
            }
            if (!bFoundSomething) {
               CmdLineLeftList.Add(Params[0]);
               Params.RemoveAt(0);
            }
         }
      }

      /// <summary>
      /// Zwraca opisy wszystkich opcji.
      /// </summary>
      /// <returns>Wynikiem jest sformatowany napis zawierający opisy
      /// wszystkich opcji programu.</returns>
      public string GetExplanations()
      {
         int nMaxWidth = 0;
         foreach(CustomOption Option in OptionList)
            nMaxWidth = Math.Max(nMaxWidth, Option.OptionWidth);
         StringBuilder Result = new StringBuilder();
         for (int nInd = 0; nInd < OptionList.Count; nInd++)
            Result.Append(OptionList[nInd].GetExplanation(nMaxWidth));
         return Result.ToString();
      }

      /// <summary>
      /// Wyświetla opis wszystkich opcji.
      /// </summary>
      /// <remarks>
      /// Metoda wyświetla opis wszystkich opcji aplikacji na konsoli znakowej.
      /// </remarks>
      public void WriteExplanations()
      {
         int nMaxWidth = 0;
         foreach(CustomOption Option in OptionList)
            nMaxWidth = Math.Max(nMaxWidth, Option.OptionWidth);
         for (int nInd = 0; nInd < OptionList.Count; nInd++)
            OptionList[nInd].WriteExplanation(nMaxWidth);
      }
   }

}
