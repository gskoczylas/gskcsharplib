﻿# GSkCSharpLib

Package of general purpose classes.

- Language: C#

## Source code documentation

All documentation comments are written in Polish. If your are interested in
documentation in English, please contact me (mailto:gskoczylas+GSkCSharpLib@gmail.com.pl).