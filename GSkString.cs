﻿/**********************************************
 *                                            *
 *                  GSkC#Lib                  *
 *     ——————————————————————————————————     *
 *  Copyright © 2009-2025 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************/


using System;
using System.Text;
using System.Collections.Specialized;

/// <summary>
/// 
/// </summary>
namespace GSkTools.StringUtils
{

   static public class StrUtils
   {

      /// <summary>
      /// Funkcja zwraca napis składający się ze wskazanej liczby odstępów.
      /// </summary>
      /// <param name="nCount">Liczba odstępów.</param>
      static public string Spaces(int nCount)
      {
         return "".PadRight(nCount);
      }
      
      /// <summary>
      /// Funkcja zwraca napis składający się ze wskazanej liczby znaków.
      /// </summary>
      /// <param name="chVal">Znak, który należy powtórzyć.</param>
      /// <param name="nCount">Liczba powtórzeń.</param>
      /// <returns>Napis, w którym wskazany <paramref name="chVal">znak</paramref>
      /// jest powtórzony odpowiednią <paramref name="nCount">liczbę</paramref>
      /// razy.</returns>
      static public string Replicate(char chVal,
                                     int nCount)
      {
         return new String(chVal, nCount);
      }
      
      /// <summary>
      /// Funkcja zwraca napis powtórzony odpowidnią liczbę razy.
      /// </summary>
      /// <param name="sText">Tekst, który należy powtórzyć.</param>
      /// <param name="nCount">Liczba powtórzeń.</param>
      /// <returns>Napis, w którym wskazany <paramref name="sTekst">tekst</paramref>
      /// jest powtórzony odpowiednią <paramref name="nCount">liczbę</paramref>
      /// razy.</returns>
      static public string Replicate(string sText,
                                     int nCount)
      {
         StringBuilder sResult = new StringBuilder(sText.Length * nCount);
         for (int nInd = nCount; nInd > 0; nInd--)
            sResult = sResult.Append(sText);
         return sResult.ToString();
      }

      /// <summary>
      /// Funkcja dzieli wskazany napis na wiersze nie przekraczające wskazanej
      /// długości.
      /// </summary>
      /// <param name="sText">Tekst do podzielenia.</param>
      /// <param name="nWidth">Maksymalna długość wierszy (w znakach).</param>
      /// <returns>Wynikiem funkcji jest tablica wierszy.</returns>
      /// <remarks>Funkcja zakłada, że napis wejściowy jest pojedynczym wierszem.
      /// Funkcja uwzględnia znaki '\n' i '\r' traktując je jako znaki zmiany
      /// wiersza. Znaki tabulacji traktowane są jak pojedynczy znak.</remarks>
      static public string[] Wrap(this string sText,
                                  int nWidth)
      {
         int nInd;
         StringCollection sResult = new StringCollection();
         string[] asResult = sText.Split('\n', '\r');
         if (asResult.Length > 1) {
            for (nInd = 0; nInd < asResult.Length; nInd++) {
               string[] asTemp = asResult[nInd].Wrap(nWidth);
               for (int i = 0; i < asTemp.Length; i++)
                  sResult.Add(asTemp[i]);
            }
         } else {
            sText = sText.Trim(' ');
            while (sText.Length > nWidth) {
               nInd = nWidth;
               do {
                  nInd--;
               } while (nInd >= 0
                        && Char.IsLetterOrDigit(sText, nInd));
               if (nInd < 0)
                  nInd = nWidth - 1;
               sResult.Add(sText.Substring(0, nInd).TrimEnd(' '));
               sText = sText.Remove(0, nInd).TrimStart(' ');
            }
            if (sText.Length != 0)
               sResult.Add(sText);
         }
         asResult = new string[sResult.Count];
         for (nInd = 0; nInd < sResult.Count; nInd++)
            asResult[nInd] = sResult[nInd];
         return asResult;
      }
   }
}
