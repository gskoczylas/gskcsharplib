﻿/**********************************************
 *                                            *
 *                  GSkC#Lib                  *
 *     ——————————————————————————————————     *
 *  Copyright © 2009-2025 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************/


using System;
using System.Reflection;
using System.Text;

namespace GSkTools.Log
{

    public enum LogEventLevel { Verbose, Debug, Information, Warning, Error, Fatal }  // copied from the SeriLog package


    public interface ICustomLogger
    {
        string LogBaseFilePath {
            get; set;
        }

        string LogFullFilePath {
            get; 
        }

        LogEventLevel LogLevel {
            get; set;
        }

        ICustomLogger Enter(string message,
                            LogEventLevel level = LogEventLevel.Information);
        ICustomLogger Enter(string format,
                            object param,
                               LogEventLevel level = LogEventLevel.Information);
        ICustomLogger Enter(string format,
                            object[] paramList,
                            LogEventLevel level = LogEventLevel.Information);

        ICustomLogger Message(string message,
                              LogEventLevel level = LogEventLevel.Information);
        ICustomLogger Message(string format,
                              object param,
                              LogEventLevel level = LogEventLevel.Information);
        ICustomLogger Message(string format,
                              object[] paramList,
                              LogEventLevel level = LogEventLevel.Information);

        ICustomLogger Value(string name,
                            object value,
                            LogEventLevel level = LogEventLevel.Information);

        ICustomLogger ValueHex(string name,
                               uint value,
                               LogEventLevel level = LogEventLevel.Information);

        ICustomLogger Result(object value,
                             LogEventLevel level = LogEventLevel.Information);

        ICustomLogger ResultHex(uint value,
                                LogEventLevel level = LogEventLevel.Information);

        ICustomLogger Exception(Exception value,
                                LogEventLevel level = LogEventLevel.Error);

        ICustomLogger Exit(string message,
                           LogEventLevel level = LogEventLevel.Information);
        ICustomLogger Exit(string format,
                           object param,
                           LogEventLevel level = LogEventLevel.Information);
        ICustomLogger Exit(string format,
                           object[] paramList,
                           LogEventLevel level = LogEventLevel.Information);
    }

    abstract public class CustomLoggerBase : ICustomLogger
    {
        public const int LOG_FILE_SIZE_LIMIT = 1024 * 1024 * 32;   // 32 MiB

        protected int Offset {
            get; set;
        }

        public virtual string LogBaseFilePath {
            get;
            set;
        }

        public string LogFullFilePath {
            get {
                return LogBaseFilePath + ".log";
            }
        }

        public LogEventLevel LogLevel {
            get; set;
        }

        public CustomLoggerBase()
        {
            LogBaseFilePath = Assembly.GetCallingAssembly().GetName().Name;
            LogLevel = LogEventLevel.Verbose;
        }

        public ICustomLogger Enter(string message,
                                   LogEventLevel level = LogEventLevel.Information)
        {
            Offset += 2;
            Message(message + " - START", level);
            Offset += 2;
            return this;
        }

        public ICustomLogger Enter(string format,
                                   object param,
                                   LogEventLevel level = LogEventLevel.Information)
        {
            return Enter(format, new object[] { param }, level);
        }

        public ICustomLogger Enter(string format,
                                   object[] paramList,
                                   LogEventLevel level = LogEventLevel.Information)
        {
            return Enter(string.Format(format, paramList), level);
        }

        public abstract ICustomLogger Message(string message,
                                              LogEventLevel level = LogEventLevel.Information);

        public ICustomLogger Message(string format,
                                     object param,
                                     LogEventLevel level = LogEventLevel.Information)
        {
            return Message(format, new object[] { param }, level);
        }

        public ICustomLogger Message(string format,
                                     object[] paramList,
                                     LogEventLevel level = LogEventLevel.Information)
        {
            return Message(string.Format(format, paramList), level);
        }

        public ICustomLogger Value(string name,
                                   object value,
                                   LogEventLevel level = LogEventLevel.Information)
        {
            string valueStr;
            if (value == null)
                valueStr = "null";
            else if (value is string) 
                valueStr = '"' + (string)value + '"';       
            else {
                valueStr = value.ToString();
                if (valueStr.Length == 0)
                    valueStr = "{}";
            }
            return Message(name + " = " + valueStr, level);
        }

        public ICustomLogger ValueHex(string name,
                                      uint value,
                                      LogEventLevel level = LogEventLevel.Information)
        {
            return Message(string.Format("{0} = {1} // 0x{1:X}",
                                         args: new object[] { name, value }),
                           level);
        }

        public ICustomLogger Result(object value,
                                    LogEventLevel level = LogEventLevel.Information)
        {
            return Value("Result", value, level);
        }

        public ICustomLogger ResultHex(uint value,
                                       LogEventLevel level = LogEventLevel.Information)
        {
            return ValueHex("Result", value, level);
        }

        public ICustomLogger Exception(Exception value,
                                       LogEventLevel level = LogEventLevel.Error)
        {
            return Enter("Exception " + value.GetType().Name + ": " + value.Message)
                   .Message(value.ToString())
                   .Exit("Exception");
        }

        public ICustomLogger Exit(string message,
                                  LogEventLevel level = LogEventLevel.Information)
        {
            Offset -= 2;
            Message(message + " - STOP", level);
            Offset -= 2;
            return this;
        }

        public ICustomLogger Exit(string format,
                                  object param,
                                  LogEventLevel level = LogEventLevel.Information)
        {
            return Exit(format, new object[] { param }, level);
        }

        public ICustomLogger Exit(string format,
                                  object[] paramList,
                                  LogEventLevel level = LogEventLevel.Information)
        {
            return Exit(string.Format(format, paramList), level);
        }

    }


    public class EmptyLogger : CustomLoggerBase
    {
        override public ICustomLogger Message(string message,
                                              LogEventLevel level = LogEventLevel.Information)
        {
            return this;
        }

    }


    public class BasicLogger : CustomLoggerBase
    {
        private class BasicLogWriter
        {
            private readonly string fileName;

            public BasicLogWriter(string BaseLogFilePath)
            {
                fileName = BaseLogFilePath;
            }

            public void Write(LogEventLevel level,
                              int offset,
                              string message)
            {
                if (System.IO.File.Exists(fileName + ".log"))
                {
                    var fileInfo = new System.IO.FileInfo(fileName + ".log");
                    if (fileInfo.Length >= LOG_FILE_SIZE_LIMIT)
                    {
                        if (System.IO.File.Exists(fileName + ".old.log"))
                            System.IO.File.Delete(fileName + ".old.log");
                        fileInfo.MoveTo(fileName + ".old.log");
                    }
                }
                using (System.IO.StreamWriter logFile = new System.IO.StreamWriter(fileName + ".log",
                                                                                   true, Encoding.UTF8))
                {
                    try
                    {
                        logFile.Write(level == LogEventLevel.Error ?   "E" :
                                      level == LogEventLevel.Warning ? "W" :
                                      level == LogEventLevel.Debug ?   "-" :
                                      level == LogEventLevel.Verbose ? "." :
                                      level == LogEventLevel.Fatal ?   "!" :
                                                                       " ");
                        logFile.Write(" ");
                        logFile.Write(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        logFile.Write(new string(' ', offset + 1));
                        logFile.Write(message);
                        logFile.WriteLine();
                        logFile.Flush();
                    }
                    finally
                    {
                        logFile.Flush();
                        logFile.Close();
                    }
                }
            }
        }

        private BasicLogWriter log;

        public BasicLogger() 
            : base()
        {            
            log = new BasicLogWriter(LogBaseFilePath);
        }

        public override string LogBaseFilePath {
            get => base.LogBaseFilePath;
            set {
                base.LogBaseFilePath = value;
                log = new BasicLogWriter(LogBaseFilePath);
            }               
        }

        public override ICustomLogger Message(string message, 
                                              LogEventLevel level = LogEventLevel.Information)
        {
            if (level >= LogLevel)
                log.Write(level, Offset, message);
            return this;
        }
    }


    //public class SimpleLogger : ISimpleLogger
    //{
    //    private ILogger log;

    //        public SimpleLogger()
    //        {
    //            var fileName = Assembly.GetCallingAssembly().GetName().Name + ".log";
    //            log = new LoggerConfiguration()
    //                .ReadFrom.AppSettings()
    //                .WriteTo.File(fileName,
    //                              rollingInterval: RollingInterval.Day,
    //                              retainedFileCountLimit: 7,
    //                              rollOnFileSizeLimit: true,
    //                              fileSizeLimitBytes: 1024 * 1024 * 1024,
    //                              encoding: Encoding.UTF8)
    //                .CreateLogger();
    //        }

    //}

}
