﻿/**********************************************
 *                                            *
 *                  GSkC#Lib                  *
 *     ——————————————————————————————————     *
 *  Copyright © 2009-2025 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************/


using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;


namespace GSkTools.Dialogs
{

   /// <summary>
   /// Uproszczone komunikaty błędów, ostrzeżeń lub pytań
   /// </summary>
   static public class MsgBox
   {
      #region Private

      /// <summary>
      /// Konwertuje numer przycisku na odpowiedni typ wyliczeniowy.
      /// </summary>
      /// <param name="nDefaultButton">Numer przycisku (1, 2 lub 3).</param>
      /// <returns>Odpowiednia wartość typu <c>MessageBoxDefaultButton</c>.</returns>
      /// <remarks>Nie można do tego używać prostego mapowania typów, ponieważ
      /// wartości typu <c>MessageBoxDefaultButton</c> mają nadane specyficzne
      /// wartości.</remarks>
      static private MessageBoxDefaultButton DefBtn(int nDefaultButton)
      {
         switch (nDefaultButton) {
            case 2:
               return MessageBoxDefaultButton.Button2;
            case 3:
               return MessageBoxDefaultButton.Button3;
            default:
               return MessageBoxDefaultButton.Button1;
         }
      }

      /// <summary>
      ///   Prywatna klasa definiująca okno dialogowe wykorzystywane w metodzie
      ///   <see cref="InputBox"/>.
      /// </summary>
      private class InputDlg : Form
      {
         private IContainer components = null;

         private Label lblPrompt;
         private TextBox edtValue;
         private Button btnOK;
         private Button btnCancel;
         private ErrorProvider errorProvider;

         public bool AllowEmpty
         {
            get;
            set;
         }

         public bool TrimValue
         {
            get;
            set;
         }

         override public string Text
         {
            get {
               return GetText();
            }
            set {
               edtValue.Text = value;
            }
         }

         public int MaxLength {
            get {
               return edtValue.MaxLength;
            }
            set {
               edtValue.MaxLength = value;
            }
         }

         public string DialogCaption
         {
            get {
               return base.Text;
            }
            set {
               base.Text = value;
            }
         }

         public string Prompt
         {
            get {
               return lblPrompt.Text;
            }
            set {
               lblPrompt.Text = value;
            }
         }

         public InputDlg()
         {
            InitializeComponent();
            TrimValue = true;
         }

         private string GetText()
         {
            string sResult = edtValue.Text;
            if (TrimValue)
               sResult = sResult.Trim();
            return sResult;
         }

         /// <summary>
         /// Clean up any resources being used.
         /// </summary>
         /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
         protected override void Dispose(bool disposing)
         {
            if (disposing && (components != null)) {
               components.Dispose();
            }
            base.Dispose(disposing);
         }

         /// <summary>
         /// Required method for Designer support - do not modify
         /// the contents of this method with the code editor.
         /// </summary>
         private void InitializeComponent()
         {
            components = new Container();
            lblPrompt = new Label();
            edtValue = new TextBox();
            btnOK = new Button();
            btnCancel = new Button();
            errorProvider = new ErrorProvider(components);
            ((ISupportInitialize)(errorProvider)).BeginInit();
            SuspendLayout();
            // 
            // lblPrompt
            // 
            lblPrompt.AutoSize = true;
            lblPrompt.Location = new Point(23, 17);
            lblPrompt.Name = "lblPrompt";
            lblPrompt.Size = new Size(62, 13);
            lblPrompt.TabIndex = 0;
            lblPrompt.Text = "Napisz coś";
            // 
            // edtValue
            // 
            edtValue.Location = new Point(26, 33);
            edtValue.Name = "edtValue";
            edtValue.Size = new Size(600, 20);
            edtValue.TabIndex = 1;
            edtValue.Validating += new CancelEventHandler(edtValue_Validating);
            // 
            // btnOK
            // 
            btnOK.DialogResult = DialogResult.OK;
            btnOK.Location = new Point(469, 69);
            btnOK.Name = "btnOK";
            btnOK.Size = new Size(75, 23);
            btnOK.TabIndex = 2;
            btnOK.Text = "OK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = DialogResult.Cancel;
            btnCancel.Location = new Point(551, 69);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(75, 23);
            btnCancel.TabIndex = 3;
            btnCancel.Text = "Anuluj";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // errorProvider
            // 
            errorProvider.ContainerControl = this;
            // 
            // frmAsk
            // 
            AcceptButton = btnOK;
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            CancelButton = btnCancel;
            ClientSize = new Size(647, 112);
            Controls.Add(btnCancel);
            Controls.Add(btnOK);
            Controls.Add(edtValue);
            Controls.Add(lblPrompt);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "frmAsk";
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterParent;
            Text = "Pytanie";
            Shown += new System.EventHandler(this.frmAsk_Shown);
            ((ISupportInitialize)(errorProvider)).EndInit();
            ResumeLayout(false);
            PerformLayout();
         }

         private void edtValue_Validating(object sender, CancelEventArgs e)
         {
            if (!AllowEmpty
                && GetText().Equals(""))
               errorProvider.SetError(edtValue, "Wpisz wartość');
            else
               errorProvider.SetError(edtValue, "");
            btnOK.Enabled = errorProvider.GetError(edtValue).Equals("");
         }

         private void frmAsk_Shown(object sender, EventArgs e)
         {
            btnOK.Enabled = AllowEmpty
                            || GetText() != "";
         }
      }

      #endregion

      #region Error

      /// <summary>Wyświetla standardowy komunikat błędu.</summary>
      /// <param name="sErrMsg">Treść komunikatu błędu.</param>
      /// <remarks>Okno komunikatu ma standardowy tytuł.</remarks>
      static public void Error(string sErrMsg)
      {
         Error("Błąd", sErrMsg);
      }

      /// <summary>
      /// Wyświetla standardowy komunikat błędu.
      /// </summary>
      /// <param name="sFormat">Format komunikatu błędu.</param>
      /// <param name="Arguments">Parametry komunikatu błędu.</param>
      /// <example>MsgBox.Error("'{0}' nie jest poprawną liczbą", textBox1.Text);</example>
      static public void Error(string sFormat,
                               params object[] Arguments)
      {
         Error(string.Format(sFormat, Arguments));
      }

      /// <summary>Wyswietla standardowy komunikat błędu.</summary>
      /// <param name="sCaption">Treść tytułu okna.</param>
      /// <param name="sErrMsg">Treść komunikatu błędu</param>
      static public void Error(string sCaption,
                               string sErrMsg)
      {
         MessageBox.Show(sErrMsg, sCaption,
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

      /// <summary>
      /// Wyświetla komunikat błędu spowodowany wyjątkiem w aplikacji.
      /// </summary>
      /// <param name="sCaption">Tytuł okna z komunikatem błędu.</param>
      /// <param name="sFriendlyMessage">Treść komunikatu związanego
      /// z wyjątkiem w firmie zrozumiałej dla użytkownika aplikacji.</param>
      /// <param name="eError">Wyjątek wygeneroany przez aplikację.</param>
      static public void Error(string sCaption,
                               string sFriendlyMessage,
                               Exception eError)
      {
         Error(sCaption,
               string.Format("{0}\n\nOryginalny komunikat błędu:\n{1}\n\nPoinformuj autora programu o treści tego komunikatu.",
                             sFriendlyMessage, eError.Message));
      }

      /// <summary>
      /// Wyświetla komunikat błędu spowodowany wyjątkiem w aplikacji.
      /// </summary>
      /// <param name="sFriendlyMessage">Treść komunikatu związanego 
      ///   z wyjąkiem w formie zrozumiałej dla użytkownika aplikacki.</param>
      /// <param name="eError">Wyjątek wygenerowany przez aplikację.</param>
      static public void Error(string sFriendlyMessage,
                               Exception eError)
      {
         Error(sFriendlyMessage, eError);
      }

      /// <summary>
      /// Wyświetla komunikat błędu spowodowany wyjątkiem w aplikacji.
      /// </summary>
      /// <param name="eError">Wyjątek wygenerowany przez aplikację.</param>
      /// <remarks>Ta wersja jest równoważna wywołaniu <code>Error("Błąd", eError)</code>.</remarks>
      static public void Error(Exception eError)
      {
         Error("Błąd aplikacji", eError);
      }

      #endregion

      #region Warning

      /// <summary>
      /// Wyświetla standardowy komunikat ostrzeżenia.
      /// </summary>
      /// <param name="sWarning">Treść ostrzeżenia.</param>
      /// <remarks>Tytułem okna jest „Ostrzeżenie”.</remarks>
      static public void Warning(string sWarning)
      {
         Warning("Ostrzeżenie", sWarning);
      }

      /// <summary>
      /// Wyświetla standardowy komunikat ostrzeżenia.
      /// </summary>
      /// <param name="sFormat">Format ostrzeżenia.</param>
      /// <param name="Arguments">Parametry ostrzeżenia.</param>
      /// <example>MsgBox.Warning("'{0}' nie jest poprawą liczbą", textBox1.Text);</example>
      static public void Warning(string sFormat,
                                 params object[] Arguments)
      {
         Warning(string.Format(sFormat, Arguments));
      }

      /// <summary>
      /// Wyświetla standardowy komunikat ostrzeżenia.
      /// </summary>
      /// <param name="sCaption">Tytuł okna.</param>
      /// <param name="sWarning">Treść ostrzeżenia.</param>
      static public void Warning(string sCaption,
                                 string sWarning)
      {
         Warning(sCaption, sWarning, MessageBoxButtons.OK, 0);
      }

      /// <summary>
      /// Wyświetla standardowy komunikat ostrzeżenia.
      /// </summary>
      /// <param name="sCaption">Tytuł okna.</param>
      /// <param name="sWarning">Treść ostrzeżenia.</param>
      /// <param name="setButtons">Zestaw przyciskĂłw.</param>
      /// <param name="nDefaultButton">Numer domyślnego przycisku.</param>
      /// <remarks>Pierwszy przycisk ma numer <c>1</c>.</remarks>
      /// <returns>Przycisk wybrany przez użytkownika.</returns>
      static public DialogResult Warning(string sCaption,
                                         string sWarning,
                                         MessageBoxButtons setButtons,
                                         int nDefaultButton)
      {
         return MessageBox.Show(sWarning, sCaption, setButtons,
                                MessageBoxIcon.Warning,
                                DefBtn(nDefaultButton));
      }

      #endregion

      #region Question

      /// <summary>Wyświetla okno z pytaniem.</summary>
      /// <param name="sQuestion">Treść pytania.</param>
      /// <remarks>Użytkownik może wybrać jedną z dwóch odpowiedzi: <c>Tak</c> 
      /// lub <c>Nie</c>.</remarks>
      /// <returns><c>true</c> jeżeli użytkownik wybierze odpowiedź <c>Tak</c>.</returns>
      static public bool Question(string sQuestion)
      {
         return Question("Pytanie", sQuestion);
      }

      /// <summary>
      /// Wyświetla okno z pytaniem.
      /// </summary>
      /// <param name="sFormat">Format pytania.</param>
      /// <param name="Arguments">Parametry pytania.</param>
      /// <remarks>Użytkownik może wybrać jedną z dwóch odpowiedzi: <c>Tak</c> 
      /// lub <c>Nie</c>.</remarks>
      /// <returns><c>true</c> jeżeli użytkownik wybierze odpowiedź <c>Tak</c>.</returns>
      /// <example>if (MsgBox.Question("Czy wydrukować raport {0}?", textBox1.Text) ...</example>
      static public bool Question(string sFormat,
                                  params object[] Arguments)
      {
         return Question(string.Format(sFormat, Arguments));
      }

      /// <summary>Wyświetla okno z pytaniem.</summary>
      /// <param name="sCaption">Tytuł okna</param>
      /// <param name="sQuestion">Treść pytania.</param>
      /// <param name="Arguments">Parametry pytania.</param>
      /// <remarks>Użytkownik może wybrać jedną z dwóch odpowiedzi: <c>Tak</c> 
      /// lub <c>Nie</c>.</remarks>
      /// <returns><c>true</c> jeżeli użytkownik wybierze odpowiedź <c>Tak</c>.</returns>
      static public bool Question(string sCaption,
                                  string sQuestion,
                                  params object[] Arguments)
      {
         return Question(sCaption, string.Format(sQuestion, Arguments), true);
      }

      /// <summary>Wyświetla okno z pytaniem.</summary>
      /// <param name="sCaption">Tytuł okna.</param>
      /// <param name="sQuestion">Treść pytania.</param>
      /// <param name="bDefault">Czy domyślna odpowiedziź ma być [Tak] (<c>true</c>)
      /// czy [Nie] (<c>false</c>).</param>
      /// <returns><c>true</c> jeżeli użytkownik wybierze odpowiedź <c>Tak</c>.</returns>
      static public bool Question(string sCaption,
                                  string sQuestion,
                                  bool bDefault)
      {
         return Question(sCaption, sQuestion, 
                         MessageBoxButtons.YesNo,
                         bDefault ? 1 : 2) == DialogResult.Yes;
      }

      /// <summary>Wyświetla okno z pytaniem.</summary>
      /// <remarks>W oknie wyświetlana jest ikona z pytajnikiem.</remarks>
      /// <param name="sCaption">Tytuł okna.</param>
      /// <param name="sQuestion">Treść pytania.</param>
      /// <param name="setButtons">Zestaw przycisków w pytaniu.</param>
      /// <param name="nDefaultButton">Numer domyślnej odpowiedzi.
      /// Pierwszy przycisk ma numer <c>1</c>.</param>
      /// <returns>Wynikiem funkcji jest wybrany przycisk.</returns>
      static public DialogResult Question(string sCaption,
                                          string sQuestion,
                                          MessageBoxButtons setButtons,
                                          int nDefaultButton)
      {
         return MessageBox.Show(sQuestion, sCaption, setButtons,
                                MessageBoxIcon.Question,
                                DefBtn(nDefaultButton));
      }

      #endregion

      #region Information

      /// <summary>
      /// Wyświetla okno z informację
      /// </summary>
      /// <param name="sInfiormation">Treść informacji.</param>
      static public void Information(string sInfiormation)
      {
         Information("Informacja", sInfiormation);
      }

      /// <summary>
      /// Wyświetla informację
      /// </summary>
      /// <param name="sFormat">Format informacji.</param>
      /// <param name="Arguments">Parametry informacji.</param>
      static public void Information(string sFormat,
                                     params object[] Arguments)
      {
         Information(string.Format(sFormat, Arguments));
      }

      /// <summary>
      /// Wyświetla informację
      /// </summary>
      /// <param name="sCaption">Tytuł okna.</param>
      /// <param name="sInformation">Treść informacji.</param>
      /// <param name="Arguments">Parametry informacji.</param>
      static public void Information(string sCaption,
                                     string sInformation,
                                     params object[] Arguments)
      {
         MessageBox.Show(string.Format(sInformation, Arguments), 
                         sCaption,
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Information);
      }

      #endregion

      #region InputBox

      /// <summary>
      ///   Proste okno dialogowe z pytaniem o wartość tekstową
      /// </summary>
      /// <param name="sDialogCaption">Tytuł okna dialogowego.</param>
      /// <param name="sPrompt">Nagłówek pola tekstowego.</param>
      /// <param name="sText"><para>Przy wywołaniu parametr zawiera wartość domyślną pola tekstowego.</para>
      ///                     <para>Po powrocie z funkcji parametr przestawia tekst wpisany przez użytkownika.</para></param>
      /// <param name="bAllowEmpty"><para>Czy akceptować pusty tekst.</para>
      ///                           <para>Domyślnie: nie.</para></param>
      /// <param name="bTrimValue"><para>Czy automatycznie usuwać początkowe i końcowe napisy.</para>
      ///                          <para>Domyślnie: tak.</para></param>
      /// <param name="nMaxLength"><para>Maksymalna dopuszczalna długość napisu.</para>
      ///                          <para>Domyślnie: 32767.</para></param>
      /// <returns>
      ///   <para>
      ///     Wynikiem funkcji <c>true</c> jeżeli użytkownik zamknął dialog
      ///     przyciskiem [OK] lub klawiszem {Enter}. Wtedy zmienna przekazana
      ///     przez parametr <paramref name="sText"/> zawiera tekst zaakceptowany
      ///     przez użytkownika.</para>
      ///   <para>
      ///     Jeżeli użytkownik zamknie okno dialogowe przyciskiem [Anuluj] lub 
      ///     klawiszem {Esc} to wynikiem funkcji jest <c>false</c>. W takim 
      ///     przypadku zmienna przekazana przez parametr <paramref name="sTekst"/>
      ///     pozostanie bez zmian.</para>
      /// </returns>
      static public bool InputBox(string sDialogCaption,
                                  string sPrompt,
                                  ref string sText,
                                  bool bAllowEmpty,
                                  bool bTrimValue,
                                  int nMaxLength)
      {
         InputDlg dlgInp = new InputDlg();
         dlgInp.DialogCaption = sDialogCaption;
         dlgInp.Prompt = sPrompt;
         dlgInp.AllowEmpty = bAllowEmpty;
         dlgInp.TrimValue = bTrimValue;
         dlgInp.Text = sText;
         dlgInp.MaxLength = nMaxLength;
         if (dlgInp.ShowDialog() == DialogResult.OK) {
            sText = dlgInp.Text;
            return true;
         } 
         return false;
      }

      /// <summary>
      ///   Proste okno dialogowe z pytaniem o wartość tekstową
      /// </summary>
      /// <param name="sDialogCaption">Tytuł okna dialogowego.</param>
      /// <param name="sPrompt">Nagłówek pola tekstowego.</param>
      /// <param name="sText"><para>Przy wywołaniu parametr zawiera wartość domyślną pola tekstowego.</para>
      ///                     <para>Po powrocie z funkcji parametr przestawia tekst wpisany przez użytkownika.</para></param>
      /// <param name="bAllowEmpty"><para>Czy akceptować pusty tekst.</para>
      ///                           <para>Domyślnie: nie.</para></param>
      /// <param name="bTrimValue"><para>Czy automatycznie usuwać początkowe i końowe napisy.</para>
      ///                          <para>Domyślnie: tak.</para></param>
      /// <returns>
      ///   <para>
      ///     Wynikiem funkcji <c>true</c> jeżeli użytkownik zamknął dialog
      ///     przyciskiem [OK] lub klawiszem {Enter}. Wtedy zmienna przekazana
      ///     przez parametr <paramref name="sText"/> zawiera tekst zaakceptowany
      ///     przez użytkownika.</para>
      ///   <para>
      ///     JeĹĽeli uĹĽytkownik zamknie okno dialogowe przyciskiem [Anuluj] lub 
      ///     klawiszem {Esc} to wynikiem funkcji jest <c>false</c>. W takim 
      ///     przypadku zmienna przekazana przez parametr <paramref name="sTekst"/>
      ///     pozostanie bez zmian.</para>
      /// </returns>
      static public bool InputBox(string sDialogCaption,
                                  string sPrompt,
                                  ref string sText,
                                  bool bAllowEmpty,
                                  bool bTrimValue)
      {
         InputDlg dlgInp = new InputDlg();
         dlgInp.DialogCaption = sDialogCaption;
         dlgInp.Prompt = sPrompt;
         dlgInp.AllowEmpty = bAllowEmpty;
         dlgInp.TrimValue = bTrimValue;
         dlgInp.Text = sText;
         if (dlgInp.ShowDialog() == DialogResult.OK) {
            sText = dlgInp.Text;
            return true;
         }
         return false;
      }

      /// <summary>
      ///   Proste okno dialogowe z pytaniem o wartość tekstową.
      /// </summary>
      /// <param name="sDialogCaption">Tytuł okna dialogowego.</param>
      /// <param name="sPrompt">Nagłówek pola tekstowego.</param>
      /// <param name="sText"><para>Przy wywołaniu parametr zawiera wartość domyślną pola tekstowego.</para>
      ///                     <para>Po powrocie z funkcji parametr przestawia tekst wpisany przez użytkownika.</para></param>
      /// <returns>
      ///   <para>
      ///     Wynikiem funkcji <c>true</c> jeżeli użytkownik zamknął dialog
      ///     przyciskiem [OK] lub klawiszem {Enter}. Wtedy zmienna przekazana
      ///     przez parametr <paramref name="sText"/> zawiera tekst zaakceptowany
      ///     przez użytkownika.</para>
      ///   <para>
      ///     Jeżeli użytkownik zamknie okno dialogowe przyciskiem [Anuluj] lub 
      ///     klawiszem {Esc} to wynikiem funkcji jest <c>false</c>. W takim 
      ///     przypadku zmienna przekazana przez parametr <paramref name="sTekst"/>
      ///     pozostanie bez zmian.</para>
      /// </returns>
      /// <remarks>
      ///   W tej wersji funkcji pusty tekst nie jest dopuszczalny. 
      ///   Ponadto z tekstu wprowadzonego przez użytkownika automatycznie
      ///   usuwane sa początkowe i końcowe odstępy. 
      /// </remarks>
      static public bool InputBox(string sDialogCaption,
                                  string sPrompt,
                                  ref string sText)
      {
         InputDlg dlgInp = new InputDlg();
         dlgInp.DialogCaption = sDialogCaption;
         dlgInp.Prompt = sPrompt;
         dlgInp.Text = sText;
         if (dlgInp.ShowDialog() == DialogResult.OK) {
            sText = dlgInp.Text;
            return true;
         }
         return false;
      }

      /// <summary>
      ///   Proste okno dialogowe z pytaniem o wartość tekstową
      /// </summary>
      /// <param name="sPrompt">Nagłówek pola tekstowego.</param>
      /// <param name="sText"><para>Przy wywołaniu parametr zawiera wartość domyślną pola tekstowego.</para>
      ///                     <para>Po powrocie z funkcji parametr przestawia tekst wpisany przez użytkownika.</para></param>
      /// <returns>
      ///   <para>
      ///     Wynikiem funkcji <c>true</c> jeżeli użytkownik zamknął dialog
      ///     przyciskiem [OK] lub klawiszem {Enter}. Wtedy zmienna przekazana
      ///     przez parametr <paramref name="sText"/> zawiera tekst zaakceptowany
      ///     przez użytkownika.</para>
      ///   <para>
      ///     Jeżeli użytkownik zamknie okno dialogowe przyciskiem [Anuluj] lub 
      ///     klawiszem {Esc} to wynikiem funkcji jest <c>false</c>. W takim 
      ///     przypadku zmienna przekazana przez parametr <paramref name="sTekst"/>
      ///     pozostanie bez zmian.</para>
      /// </returns>
      /// <remarks>
      ///   W tej wersji funkcji pusty tekst nie jest dopuszczalny. 
      ///   Ponadto z tekstu wprowadzonego przez użytkownika automatycznie
      ///   usuwane sa początkowe i końcowe odstępy. 
      /// </remarks>
      static public bool InputBox(string sPrompt,
                                  ref string sText)
      {
         InputDlg dlgInp = new InputDlg();
         dlgInp.Prompt = sPrompt;
         dlgInp.Text = sText;
         if (dlgInp.ShowDialog() == DialogResult.OK) {
            sText = dlgInp.Text;
            return true;
         }
         return false;
      }

      #endregion

   }
}
