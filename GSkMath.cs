﻿/**********************************************
 *                                            *
 *                  GSkC#Lib                  *
 *     ——————————————————————————————————     *
 *  Copyright © 2009-2025 Grzegorz Skoczylas  *
 *                                            *
 *         mgr Grzegorz Skoczylas             *
 *             ul. Grażyny 13/15              *
 *             43-300 Bielsko-Biała           *
 *             tel. 503 064 953               *
 *             gskoczylas@gmail.com           *
 *                                            *
 **********************************************/


using System;
using System.Drawing;
using GSkTools.General;

namespace GSkTools.MathUtils
{

   static public class MathUtils
   {

      #region CompareWith

      /// <summary>
      ///   Porównuje wartości z zadaną dokładnością.
      /// </summary>
      /// <param name="nVal2">Wartość, z którą następuje porównanie.</param>
      /// <param name="nEpsilon">Dokładność porównywania wartości.</param>
      /// <returns>
      /// Wynikiem porównania jest informacja o relacji między
      /// porównywanymi wartościami. Badanie na równość porównywanych
      /// wartości jest wykonywane ze wskazaną precyzją.
      /// </returns>
      /// <seealso cref="GSkTools.Math.Relationship"/>
      static public Relationship CompareWith(this decimal nValue,
                                             decimal nVal2,
                                             decimal nEpsilon)
      {
         if (System.Math.Abs(nVal2 - nValue) <= nEpsilon)
            return Relationship.EqualToValue;
         else if (nValue < nVal2)
            return Relationship.LessThanValue;
         else
            return Relationship.GreaterThanValue;
      }

      /// <summary>
      ///   Porównuje wartości z zadaną dokładnością.
      /// </summary>
      /// <param name="nVal2">Wartość, z którą następuje porównanie.</param>
      /// <param name="nEpsilon">Dokładność porównywania wartości.</param>
      /// <returns>
      ///   Wynikiem porównania jest informacja o relacji między 
      ///   porównywanymi wartościami. Badanie na równość porównywanych
      ///   wartości jest wykonywane ze wskazaną precyzją.
      /// </returns>
      /// <seealso cref="GSkTools.Math.Relationship"/>
      static public Relationship CompareWith(this double nValue,
                                             double nVal2,
                                             double nEpsilon)
      {
         if (System.Math.Abs(nVal2 - nValue) <= nEpsilon)
            return Relationship.EqualToValue;
         else if (nValue < nVal2)
            return Relationship.LessThanValue;
         else
            return Relationship.GreaterThanValue;
      }

      /// <summary>
      ///   Porównuje wartości z zadaną dokładnością.
      /// </summary>
      /// <param name="nVal2">Wartość, z którą następuje porównanie.</param>
      /// <param name="nEpsilon">Dokładność porównywania wartości.</param>
      /// <returns>
      ///   Wynikiem porównania jest informacja o relacji między 
      ///   porównywanymi wartościami. Badanie na równość porównywanych
      ///   wartości jest wykonywane ze wskazaną precyzją.
      /// </returns>
      /// <seealso cref="GSkTools.Math.Relationship"/>
      static public Relationship CompareWith(this float nValue,
                                             float nVal2,
                                             float nEpsilon)
      {
         if (System.Math.Abs(nVal2 - nValue) <= nEpsilon)
            return Relationship.EqualToValue;
         else if (nValue < nVal2)
            return Relationship.LessThanValue;
         else
            return Relationship.GreaterThanValue;
      }

      #endregion

      #region IsEqualTo

      /// <summary>
      ///   Sprawdza, czy liczby są sobie równe, z zadaną dokładnością.
      /// </summary>
      /// <param name="nVal2">Wartość, z którą następuje porównanie.</param>
      /// <param name="nEpsilon">Dokładność porównywania wartości.</param>
      /// <returns>
      ///   Wynikiem funkcji jest <c>true</c> jeżeli porównywane wartości
      ///   nie różnią się od siebie o więcej niż <paramref name="nEpsilon"/>.
      /// </returns>
      static public bool IsEqualTo(this decimal nValue,
                                   decimal nValue2,
                                   decimal nEpsilon)
      {
         return nValue.CompareWith(nValue2, nEpsilon) == Relationship.EqualToValue;
      }

      /// <summary>
      ///   Sprawdza, czy liczby są sobie równe, z zadaną dokładnością.
      /// </summary>
      /// <param name="nVal2">Wartość, z którą następuje porównanie.</param>
      /// <param name="nEpsilon">Dokładność porównywania wartości.</param>
      /// <returns>
      ///   Wynikiem funkcji jest <c>true</c> jeżeli porównywane wartości
      ///   nie różnią się od siebie o więcej niż <paramref name="nEpsilon"/>.
      /// </returns>
      static public bool IsEqualTo(this double nValue,
                                   double nValue2,
                                   double nEpsilon)
      {
         return nValue2.CompareWith(nValue2, nEpsilon) == Relationship.EqualToValue;
      }

      /// <summary>
      ///   Sprawdza, czy liczby są sobie równe, z zadaną dokładnością.
      /// </summary>
      /// <param name="nVal2">Wartość, z którą następuje porównanie.</param>
      /// <param name="nEpsilon">Dokładność porównywania wartości.</param>
      /// <returns>
      ///   Wynikiem funkcji jest <c>true</c> jeżeli porównywane wartości
      ///   nie różnią się od siebie o więcej niż <paramref name="nEpsilon"/>.
      /// </returns>
      static public bool IsEqualTo(this float nValue,
                                   float nValue2,
                                   float nEpsilon)
      {
         return nValue2.CompareWith(nValue2, nEpsilon) == Relationship.EqualToValue;
      }

      #endregion

      #region IsZero

      /// <summary>
      ///   Sprawdza, czy liczba jest równa zero, z zadaną dokładnością.
      /// </summary>
      /// <param name="nEpsilon">Dokładność sprawdzania.</param>
      static public bool IsZero(this decimal nValue,
                                decimal nEpsilon)
      {
         return nValue.IsEqualTo(0M, nEpsilon);
      }

      /// <summary>
      ///   Sprawdza, czy liczba jest równa zero, z zadaną dokładnością.
      /// </summary>
      /// <param name="nEpsilon">Dokładność sprawdzania.</param>
      static public bool IsZero(this double nValue,
                                double nEpsilon)
      {
         return nValue.IsEqualTo(0D, nEpsilon);
      }

      /// <summary>
      ///   Sprawdza, czy liczba jest równa zero, z zadaną dokładnością.
      /// </summary>
      /// <param name="nEpsilon">Dokładność sprawdzania.</param>
      static public bool IsZero(this float nValue,
                                float nEpsilon)
      {
         return nValue.IsEqualTo(0F, nEpsilon);
      }

      #endregion

      #region InRange

      /// <summary>
      ///   Funkcja rozszerzająca, sprawdzająca czy wartość należy do wskazanego
      ///   przedziału.
      /// </summary>
      /// <param name="nValue">Porównywana wartość.</param>
      /// <param name="nMinValue">Początek przedziału.</param>
      /// <param name="nMaxValue">Koniec przedziału.</param>
      /// <returns><c>true</c> jeżeli wartość należy do przedziału.</returns>
      /// <remarks>
      ///   Jeżeli wartość parametru <paramref name="nMinValue"/> jest większa
      ///   niż wartość parametru <paramref name="nMaxValue"/> to wynikiem 
      ///   funkcji zawsze jest <c>false</c>.
      /// </remarks>
      static public bool InRange(this int nValue,
                                 int nMinValue,
                                 int nMaxValue)
      {
         return nMinValue <= nValue
                && nValue <= nMaxValue;
      }

      /// <summary>
      ///   Funkcja rozszerzająca, sprawdzająca czy wartość należy do wskazanego
      ///   przedziału.
      /// </summary>
      /// <param name="nValue">Porównywana wartość.</param>
      /// <param name="nMinValue">Początek przedziału.</param>
      /// <param name="nMaxValue">Koniec przedziału.</param>
      /// <returns><c>true</c> jeżeli wartość należy do przedziału.</returns>
      /// <remarks>
      ///   Jeżeli wartość parametru <paramref name="nMinValue"/> jest większa
      ///   niż wartość parametru <paramref name="nMaxValue"/> to wynikiem 
      ///   funkcji zawsze jest <c>false</c>.
      /// </remarks>
      static public bool InRange(this decimal nValue,
                                 decimal nMinValue,
                                 decimal nMaxValue)
      {
         return nMinValue <= nValue
                && nValue <= nMaxValue;
      }

      /// <summary>
      ///   Funkcja rozszerzająca, sprawdzająca czy wartość należy do wskazanego
      ///   przedziału.
      /// </summary>
      /// <param name="nValue">Porównywana wartość.</param>
      /// <param name="nMinValue">Początek przedziału.</param>
      /// <param name="nMaxValue">Koniec przedziału.</param>
      /// <returns><c>true</c> jeżeli wartość należy do przedziału.</returns>
      /// <remarks>
      ///   Jeżeli wartość parametru <paramref name="nMinValue"/> jest większa
      ///   niż wartość parametru <paramref name="nMaxValue"/> to wynikiem 
      ///   funkcji zawsze jest <c>false</c>.
      /// </remarks>
      static public bool InRange(this double nValue,
                                 double nMinValue,
                                 double nMaxValue)
      {
         return nMinValue <= nValue
                && nValue <= nMaxValue;
      }

      /// <summary>
      ///   Funkcja rozszerzająca, sprawdzająca czy wartość należy do wskazanego
      ///   przedziału.
      /// </summary>
      /// <param name="nValue">Porównywana wartość.</param>
      /// <param name="nMinValue">Początek przedziału.</param>
      /// <param name="nMaxValue">Koniec przedziału.</param>
      /// <returns><c>true</c> jeżeli wartość należy do przedziału.</returns>
      /// <remarks>
      ///   Jeżeli wartość parametru <paramref name="nMinValue"/> jest większa
      ///   niż wartość parametru <paramref name="nMaxValue"/> to wynikiem 
      ///   funkcji zawsze jest <c>false</c>.
      /// </remarks>
      static public bool InRange(this float nValue,
                                 float nMinValue,
                                 float nMaxValue)
      {
         return nMinValue <= nValue
                && nValue <= nMaxValue;
      }

      #endregion

      #region EnsureRange

      static public int EnsureRange(this int nValue,
                                    int nMinValue,
                                    int nMaxValue)
      {
         return nValue < nMinValue
                   ? nMinValue
                   : nValue > nMaxValue
                        ? nMaxValue
                        : nValue;
      }

      /// <summary>
      ///    Zapewnia, że wartość leży w zadanym przedziale.
      /// </summary>
      /// <param name="nValue">Analizowana wartość.</param>
      /// <param name="nMinValue">Wartość minimalna.</param>
      /// <param name="nMaxValue">Wartość maksymalna.</param>
      /// <returns>
      ///   <para>Jeżeli wartość <paramref name="nValue"/> jest 
      ///   mniejsza niż wartość minimalna, wynikiem jest wartość
      ///   minimalna.</para>
      ///   <para>Jeżeli wartość <paramref name="nValue"/> jest
      ///   większa niż wartość maksymalna, wynikiem jest wartość
      ///   maksymalna.</para>
      ///   <para>W przeciwnym razie wynikiem jest oryginalna
      ///   wartość.</para>
      /// </returns>
      static public decimal EnsureRange(this decimal nValue,
                                        decimal nMinValue,
                                        decimal nMaxValue)
      {
         return nValue < nMinValue
                   ? nMinValue
                   : nValue > nMaxValue
                        ? nMaxValue
                        : nValue;
      }

      /// <summary>
      ///    Zapewnia, że wartość leży w zadanym przedziale.
      /// </summary>
      /// <param name="nValue">Analizowana wartość.</param>
      /// <param name="nMinValue">Wartość minimalna.</param>
      /// <param name="nMaxValue">Wartość maksymalna.</param>
      /// <returns>
      ///   <para>Jeżeli wartość <paramref name="nValue"/> jest 
      ///   mniejsza niż wartość minimalna, wynikiem jest wartość
      ///   minimalna.</para>
      ///   <para>Jeżeli wartość <paramref name="nValue"/> jest
      ///   większa niż wartość maksymalna, wynikiem jest wartość
      ///   maksymalna.</para>
      ///   <para>W przeciwnym razie wynikiem jest oryginalna
      ///   wartość.</para>
      /// </returns>
      static public double EnsureRange(this double nValue,
                                       double nMinValue,
                                       double nMaxValue)
      {
         return nValue < nMinValue
                   ? nMinValue
                   : nValue > nMaxValue
                        ? nMaxValue
                        : nValue;
      }

      /// <summary>
      ///    Zapewnia, że wartość leży w zadanym przedziale.
      /// </summary>
      /// <param name="nValue">Analizowana wartość.</param>
      /// <param name="nMinValue">Wartość minimalna.</param>
      /// <param name="nMaxValue">Wartość maksymalna.</param>
      /// <returns>
      ///   <para>Jeżeli wartość <paramref name="nValue"/> jest 
      ///   mniejsza niż wartość minimalna, wynikiem jest wartość
      ///   minimalna.</para>
      ///   <para>Jeżeli wartość <paramref name="nValue"/> jest
      ///   większa niż wartość maksymalna, wynikiem jest wartość
      ///   maksymalna.</para>
      ///   <para>W przeciwnym razie wynikiem jest oryginalna
      ///   wartość.</para>
      /// </returns>
      static public float EnsureRange(this float nValue,
                                      float nMinValue,
                                      float nMaxValue)
      {
         return nValue < nMinValue
                   ? nMinValue
                   : nValue > nMaxValue
                        ? nMaxValue
                        : nValue;
      }

      #endregion

      #region Point.Distance

      /// <summary>
      /// Funkcja rozszerzająca, wyliczająca odległość pomiędzy dwoma punktami.
      /// </summary>
      /// <param name="value">Porównywana wartość.</param>
      /// <param name="that">Punkt docelowy.</param>
      /// <returns>Odległość między dwoma punktami.</returns>
      static public double Distance(this Point value,
                                   Point that) {
         return Math.Round(Math.Sqrt(Math.Pow(that.X - value.X, 2) + Math.Pow(that.Y - value.Y, 2)));
      }

      #endregion

      #region Odmiana

      /// <summary>
      /// Funkcja zwraca odpowiedni tekst, w zależności od przekazanej wartości.
      /// </summary>
      /// <param name="nIle">Wartość, według której wybierany jest napis, który
      /// będzie wynikiem funkcji.</param>
      /// <param name="nWersja1">Wersja napisu dla ilości '1'.</param>
      /// <param name="nWersja2">Wersja napisu dla ilości '2'.</param>
      /// <param name="nWersja5">Wersja napisu dla iliści '3'.</param>
      /// <returns>Jeden z napisów <paramref name="nWersja1"/>, 
      ///   <paramref name="nWersja2"/> lub <paramref name="nWersja3"/>,
      ///   w zależności od wartości parametru <paramref name="nIle"/>.
      /// </returns>
      /// <remarks>
      ///   <para>
      ///     Przykład użycia:
      ///     <example>
      ///       for (int i = 0; i <= 7; i++)
      ///         Console.WriteLine(i.ToString() 
      ///                           + ' ' 
      ///                           + i.Odmiana('złoty', 'złote', 'złotych'));
      ///     </example>
      ///     Powyższy przykład wyświetli następujące teksty:
      ///     <example>
      ///       0 złotych
      ///       1 złoty
      ///       2 złote
      ///       3 złote
      ///       4 złote
      ///       5 złotych
      ///       6 złotych
      ///       7 złotych
      ///     </example>
      ///   </para>
      ///   <para>
      ///     Jeżeli teksty <paramref name="Wersja2"/> i <paramref name="Wersja5"/>
      ///     są identyczne, to ostatni parametr można pominąć.
      ///   </para>
      /// </remarks>
      static public string Odmiana(this int nValue,
                                   string sWersja1,
                                   string sWersja2,
                                   string sWersja5)
      {
         return GeneralUtils.Odmiana(nValue, sWersja1, sWersja2, sWersja5);
      }
      
      /// <summary>
      /// Funkcja zwraca odpowiedni tekst, w zależności od przekazanej wartości.
      /// </summary>
      /// <param name="nIle">Wartość, według której wybierany jest napis, który
      /// będzie wynikiem funkcji.</param>
      /// <param name="nWersja1">Wersja napisu dla ilości '1'.</param>
      /// <param name="nWersja2">Wersja napisu dla ilości '2'.</param>
      /// <returns>Jeden z napisów <paramref name="nWersja1"/>, 
      ///   <paramref name="nWersja2"/>, w zależności od wartości parametru 
      ///   <paramref name="nIle"/>.
      /// </returns>
      /// <remarks>
      ///   <para>
      ///     Przykład użycia:
      ///     <example>
      ///       for (int i = 0; i <= 5; i++)
      ///         Console.WriteLine(i.ToString() + ' ' + i.Odmiana('dzień', 'dni'));
      ///     </example>
      ///     Powyższy przykład wyświetli następujące teksty:
      ///     <example>
      ///       0 dni
      ///       1 dzień
      ///       2 dni
      ///       3 dni
      ///       4 dni
      ///       5 dni
      ///     </example>
      ///   </para>
      ///   <para>
      ///     Jeżeli tekst dla ilości '5' jest inny niż dla ilości '2', to
      ///     należy użyć wersję funkcji z trzema napisami.
      ///   </para>
      /// </remarks>
      static public string Odmiana(this int nValue,
                                   string sWersja1,
                                   string sWersja2)
      {
         return GeneralUtils.Odmiana(nValue, sWersja1, sWersja2);
      }
      
      #endregion
   }

}
